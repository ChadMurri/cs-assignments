import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.shape.Line;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class HangMan extends Application {
    private Text incorrectCorrectDisplayText = new Text("");
    private Text currentState = new Text();
    private Pane pane = new Pane();
    private ArrayList<String> phrasesList = new ArrayList<>();
    private Game board;
    private int phraseCount = 0;
    private int peicesAdded = 0;
    private Shape[] bodyParts = new Shape[6];

    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
        //phrase.txt is read into the program and stored in an arraylist
        try {
            BufferedReader read = new BufferedReader(new FileReader(new File("src/phrases.txt")));
            String text;
            while((text = read.readLine()) != null){
                phrasesList.add(text);
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        //initialize the board
        board = new Game(phrasesList.get(phraseCount));

        //creating and putting the body parts in the list
        Circle head = new Circle(200, 125, 25);
        head.setFill(Color.TRANSPARENT);
        head.setStroke(Color.BLACK);
        head.setStrokeWidth(5);
        bodyParts[0] = head;

        Line torso = new Line(200, 150, 200, 250);
        torso.setStrokeWidth(5);
        bodyParts[1] = torso;

        Line rightArm = new Line(200, 160, 150, 250);
        rightArm.setStrokeWidth(5);
        bodyParts[2] = rightArm;

        Line leftArm = new Line(200, 160, 250, 250);
        leftArm.setStrokeWidth(5);
        bodyParts[3] = leftArm;

        Line rightLeg = new Line(200, 250, 250, 350);
        rightLeg.setStrokeWidth(5);
        bodyParts[4] = rightLeg;

        Line leftLeg = new Line(200, 250, 150, 350);
        leftLeg.setStrokeWidth(5);
        bodyParts[5] = leftLeg;

        // Create a Gallows and place it in the stage
        int gallowsHeight = 400;
        int beamWidth = 100;
        int mainBeamX = 100;
        Line line1 = new Line(mainBeamX, 50, 100, gallowsHeight);
        line1.setStrokeWidth(5);

        Line line2 = new Line(mainBeamX, 50, 200, 50);
        line2.setStrokeWidth(5);

        Line line3 = new Line(mainBeamX + beamWidth, 50, mainBeamX + beamWidth, 100);
        line3.setStrokeWidth(5);

        Arc arc1 = new Arc(100, gallowsHeight + 25, 50, 25, 0, 180);
        arc1.setFill(Color.WHITE);
        arc1.setStroke(Color.BLACK);
        arc1.setStrokeWidth(5);

        pane.getChildren().addAll(line1, line2, line3, arc1);


        //adding the board display
        currentState.setFont(Font.font("Courier New", 20));
        currentState.setUnderline(true);
        currentState.setLayoutY(450);
        pane.getChildren().add(currentState);

        //adding the text display for correct or incorrect guesses
        incorrectCorrectDisplayText.setLayoutY(25);
        incorrectCorrectDisplayText.setLayoutX(250);
        pane.getChildren().add(incorrectCorrectDisplayText);

        //text field for guesses to be entered
        TextField guessField = new TextField();
        guessField.setPrefWidth(300);
        Label guess = new Label("Guess here: ", guessField);
        guessField.setOnAction(e -> guessCheck(guessField.getText(), board));
        guess.setContentDisplay(ContentDisplay.RIGHT);
        guess.setLayoutY(475);
        guess.setLayoutX(10);
        pane.getChildren().add(guess);

        //new/reset game button
        Button newGameButton = new Button("New Game");
        newGameButton.setOnAction(e -> reset());
        pane.getChildren().add(newGameButton);

        Scene scene = new Scene(pane, 500, 525);
        primaryStage.setTitle("Hang Man"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }
    //the event logic when a guess is made
    private void guessCheck(String guess, Game board){
        if(board.checkGuess(guess)){
            currentState.setText(board.getDisplay());
            if(currentState.getText().toLowerCase().equals(board.getCurrentPhrase().toLowerCase())){
                incorrectCorrectDisplayText.setText("YOU WIN!!");
            }
            else {
                incorrectCorrectDisplayText.setText("Correct!");
            }
        }
        else{
            if(peicesAdded < 6){
                incorrectCorrectDisplayText.setText("Incorrect!");
                pane.getChildren().add(bodyParts[peicesAdded]);
                peicesAdded ++;
            }
            else{
                incorrectCorrectDisplayText.setText("YOU LOSE!!! HE DED!!!");
            }
        }
    }
    //handles the gui reset of the board
    private void reset(){
        phraseCount = (phraseCount + 1) % phrasesList.size();
        board = new Game(phrasesList.get(phraseCount));
        incorrectCorrectDisplayText.setText("");
        currentState.setText(board.getDisplay());
        pane.getChildren().remove(8, 7 + peicesAdded + 1);
        peicesAdded = 0;
    }
}
class Game{
    private StringBuilder display = new StringBuilder();
    private String phrase;
    private ArrayList<Character> brokenUpPhrases = new ArrayList<>();
    private ArrayList<Character> guessList = new ArrayList<>();

    Game(String phrase){
        this.phrase = phrase;
        char[] temp = phrase.toLowerCase().toCharArray();
        for(int i = 0; i < phrase.length(); i ++){
            brokenUpPhrases.add(temp[i]);
            display.append(" ");
        }
    }
    String getDisplay(){
        return display.toString();
    }
    String getCurrentPhrase(){
        return phrase;
    }
    boolean checkGuess(String guess){
        //Checks single character entries
        if(guess.length() == 1){
            char temp = guess.toLowerCase().toCharArray()[0];
            if(brokenUpPhrases.contains(temp) && !guessList.contains(temp)){
                for(int i = 0; i < phrase.length(); i++){
                    if(brokenUpPhrases.get(i) == temp){
                        display.setCharAt(i, temp);
                    }
                }
                return true;
            }
            guessList.add(temp);
        }
        //Checks full phrase guesses
        else{
            if(guess.toLowerCase().equals(phrase.toLowerCase())){
                display.replace(0, phrase.length() - 1, phrase);
                return true;
            }
        }
        return false;
    }
}
