class MyDate{
    private int day = 0;
    private int month = 0;
    private int year = 0;
    MyDate(){}
    MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }
    int getDay(){
        return day;
    }
    int getMonth(){
        return month;
    }
    int getYear(){
        return year;
    }
    void setDay(int day){
        this.day = day;
    }
    void setMonth(int month){
        this.month = month;
    }
    void setYear(int year){
        this.year = year;
    }
    public String toString(){
        String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return String.format("%s %d of year %d", months[month], day, year);
    }
}