class Faculty extends Employee{

    private String officeHours = "Idaknow";
    private String rank = "test subject";
    Faculty(){}
    Faculty(String name, String address, String phoneNumber, String email, MyDate hireDate, String office,int salary, String officeHours, String rank){
        super(name,address,phoneNumber,email, hireDate, office, salary);
        this.officeHours = officeHours;
        this.rank = rank;
    }
    public String toString(){
        return String.format("" +
                "This is a faculty member!\n" +
                "Name:            %s\n" +
                "Street Address:  %s\n" +
                "Phone Number:    %s\n" +
                "Email Address:   %s\n" +
                "Date Hired:      %s\n" +
                "Office:          %s\n" +
                "Salary:          $%d\n" +
                "Office Hours:    %s\n" +
                "Rank:            %s\n" +
                "\n", name, address, phoneNumber, email, hireDate.toString(), office, salary, officeHours, rank);
    }
    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }
    public String getOfficeHours() {
        return officeHours;
    }

    public void setOfficeHours(String officeHours) {
        this.officeHours = officeHours;
    }

}