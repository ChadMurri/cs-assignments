class Person{
    String name = "Default McDefaulty Face";
    String address = "Homeless";
    String phoneNumber = "No phone";
    String email = "Doesn't own a computer";
    Person(){}
    Person(String name, String address, String phoneNumber, String email){
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
    public String toString(){
        return String.format("" +
                "This is a person!\n" +
                "Name:            %s\n" +
                "Street Address:  %s\n" +
                "Phone Number:    %s\n" +
                "Email Address:   %s\n" +
                "\n", name, address, phoneNumber, email);
    }
    String getName(){
        return name;
    }
    String getAddress(){
        return address;
    }
    String getPhoneNumber(){
        return phoneNumber;
    }
    String getEmail(){
        return email;
    }
    void setName(String name){
        this.name = name;
    }
    void setAddress(String address){
        this.address = address;
    }
    void setEmail(String email){
        this.email = email;
    }
    void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }
}