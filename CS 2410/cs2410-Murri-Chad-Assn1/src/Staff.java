class Staff extends Employee{

    private String title = "vagavond";
    Staff(){}
    Staff(String name, String address, String phoneNumber, String email, MyDate hireDate, String office,int salary, String title){
        super(name,address,phoneNumber,email, hireDate, office, salary);
        this.title = title;
    }
    public String toString(){
        return String.format("" +
                "This is a staff member!\n" +
                "Name:            %s\n" +
                "Street Address:  %s\n" +
                "Phone Number:    %s\n" +
                "Email Address:   %s\n" +
                "Date Hired:      %s\n" +
                "Office:          %s\n" +
                "Salary:          $%d\n" +
                "Title:           %s\n" +
                "\n", name, address, phoneNumber, email, hireDate.toString(), office, salary, title);
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}