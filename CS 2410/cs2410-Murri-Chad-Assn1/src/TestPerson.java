
class TestPerson{
    public static void main(String[] args) {
        Faculty defaultCon = new Faculty();
        Person chad = new Person("Chad Murri", "25 fake street, Not a real town 51st state", "(123) 456 - 7890", "edgeyusername47@hotmail.net");
        Student dave = new Student("Dave Davidson", "25 fake street, Not a real town 51st state", "(123) 456 - 7890", "edgeyusername47@hotmail.net", "Junior");
        Employee steve = new Employee("Steve Stevensen", "25 fake street, Not a real town 51st state", "(123) 456 - 7890", "edgeyusername47@hotmail.net", new MyDate(14,7,1996), "FB 042", 100000);
        Faculty bill = new Faculty("Bill Billson", "25 fake street, Not a real town 51st state", "(123) 456 - 7890", "edgeyusername47@hotmail.net", new MyDate(14,7,1996), "FB 042", 100000, "12:00 Am - 1:15 Pm Tuesday and Thursday", "Professor");
        Staff jill = new Staff("Jill Jillison", "25 fake street, Not a real town 51st state", "(123) 456 - 7890", "edgeyusername47@hotmail.net", new MyDate(14,7,1996), "FB 042", 100000, "Janitor");
        System.out.print(chad.toString());
        System.out.print(dave.toString());
        System.out.print(steve.toString());
        System.out.print(bill.toString());
        System.out.print(jill.toString());
        System.out.print(defaultCon.toString());
    }
}