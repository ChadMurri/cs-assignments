class Employee extends Person{
    MyDate hireDate = new MyDate();
    String office = "the dumpster outside old main";
    int salary = 0;
    Employee(){}
    Employee(String name, String address, String phoneNumber, String email, MyDate hireDate, String office,int salary){
        super(name,address,phoneNumber,email);
        this.hireDate = hireDate;
        this.office = office;
        this.salary = salary;
    }
    public String toString(){
        return String.format("" +
                "This is an Employee!\n" +
                "Name:            %s\n" +
                "Street Address:  %s\n" +
                "Phone Number:    %s\n" +
                "Email Address:   %s\n" +
                "Date Hired:      %s\n" +
                "Office:          %s\n" +
                "Salary:          $%d\n" +
                "\n", name, address, phoneNumber, email, hireDate.toString(), office, salary);
    }
    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }
    public MyDate getHireDate() {
        return hireDate;
    }

    public void setHireDate(MyDate hireDate) {
        this.hireDate = hireDate;
    }

}