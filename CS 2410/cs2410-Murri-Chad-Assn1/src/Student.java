class Student extends Person{
    private String schoolClass = "doesn't even go to School";
    Student(){}
    Student(String name, String address, String phoneNumber, String email, String schoolClass){
        super(name,address,phoneNumber,email);
        this.schoolClass = schoolClass;
    }
    public String toString(){
        return String.format("" +
                "This is a student!\n" +
                "Name:            %s\n" +
                "Street Address:  %s\n" +
                "Phone Number:    %s\n" +
                "Email Address:   %s\n" +
                "School Class:    %s\n" +
                "\n", name, address, phoneNumber, email, schoolClass);
    }
    String getSchoolClass(){
        return schoolClass;
    }
    void setSchoolClass(String schoolClass){
        this.schoolClass = schoolClass;
    }
}