import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Driver extends Application{
    private ArrayList<String> phrasesList = new ArrayList<>();
    private int currentMultiplier = 1;
    private int phraseIndex = 0;
    private int intScore = 0;
    private Text score = new Text();
    private Text currentMultDisplay = new Text();
    private TextHidden unknown;
    private WheelPane wheel;
    private boolean guessAlreadyFlag = true;
    private Text incorrect = new Text();
    @Override
    //imports the words from phrases.txt into the game
    public void start(Stage primaryStage) {
        //phrase.txt is read into the program and stored in an arraylist
        try {
            BufferedReader read = new BufferedReader(new FileReader(new File("src/phrases.txt")));
            String text;
            while ((text = read.readLine()) != null) {
                phrasesList.add(text);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //local variables
        int wheelCenterX = 250;
        int WheelCenterY = 170;
        int wheelRadius = 150;
        //create the pane
        Pane pane = new Pane();
        pane.setBackground(new Background(new BackgroundFill(Color.rgb(200, 200 , 200), CornerRadii.EMPTY, Insets.EMPTY)));
        //adding the unknown text box things
        unknown = new TextHidden(25, 350, phrasesList.get(phraseIndex));
        pane.getChildren().add(unknown);
        //add the score box
        score.setText("Score: $" + intScore);
        score.setFont(new Font(30));
        score.setX(wheelCenterX + wheelRadius + 30);
        score.setY(WheelCenterY + 10);
        pane.getChildren().add(score);
        //display the current multiplier
        currentMultDisplay.setText("Current Multiplier: spin!");
        currentMultDisplay.setFont(new Font(10));
        currentMultDisplay.setX(wheelCenterX + wheelRadius + 30);
        currentMultDisplay.setY(WheelCenterY + 30);
        pane.getChildren().add(currentMultDisplay);
        //place the wheel and the center button
        wheel = new WheelPane(wheelRadius);
        wheel.setLayoutX(wheelCenterX - wheelRadius);
        wheel.setLayoutY(WheelCenterY - wheelRadius);
        pane.getChildren().add(wheel);
        CenterCircle center = new CenterCircle(wheelCenterX, WheelCenterY);
        center.setOnMouseClicked(e -> {
            guessAlreadyFlag = false;
            wheel.makeTheWheelFuckingZoopDeWoopMyBOI();
            currentMultiplier = wheel.getScoreFromRot();
            currentMultDisplay.setText("Current Multiplier: $" + currentMultiplier + " per correct letter!");
            if (currentMultiplier == 0) {
                incorrect.setText("Bankrupt!!!");
                intScore = 0;
                setScore();
            }
        });
        pane.getChildren().add(center);
        //the pointer
        Polygon pointer = new Polygon();
        pointer.getPoints().addAll(
                (double) (wheelCenterX + wheelRadius - 25), (double) WheelCenterY,
                (double) (wheelCenterX + wheelRadius + 25), (double) (WheelCenterY - 25),
                (double) (wheelCenterX + wheelRadius + 25), (double) (WheelCenterY + 25)
        );
        pointer.setFill(Color.RED);
        pointer.setStroke(Color.BLACK);
        pointer.setStrokeWidth(3);
        pane.getChildren().add(pointer);
        //text entry
        TextField guessField = new TextField();
        guessField.setOnAction(e -> {
            if(currentMultiplier != 1) {
                if (!guessAlreadyFlag && currentMultiplier != 0) {
                    makeGuess(guessField.getText());
                    guessAlreadyFlag = true;
                }
            }
            else{
                incorrect.setText("You have to spin the wheel first!");
            }
        });
        guessField.setLayoutY(400);
        guessField.setLayoutX(130);
        pane.getChildren().add(guessField);
        //Text that yells at you if you get it wrong
        incorrect.setText("");
        incorrect.setFont(new Font(20));
        incorrect.setY(418);
        incorrect.setX(283);
        pane.getChildren().add(incorrect);
        //text box next to the guess field
        Text guessHere = new Text();
        guessHere.setText("Guess Here:");
        guessHere.setFont(new Font(20));
        guessHere.setX(10);
        guessHere.setY(418);
        pane.getChildren().add(guessHere);
        //drop down for game reset
        ComboBox<String> menu = new ComboBox<>();
        menu.getItems().add("Menu");
        menu.valueProperty().setValue("Menu");
        menu.getItems().add("Reset / New Game");
        menu.setOnAction(e -> {
            if(menu.getValue() != null){
                if(menu.getValue().equals("Reset / New Game")){
                    reset();
                    menu.valueProperty().setValue("Menu");
                }
            }
        });
        pane.getChildren().add(menu);
        //launch app
        Scene scene = new Scene(pane, 630, 470);
        primaryStage.setTitle("Wheel Of Fortune!"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }
    private void makeGuess(String guess){
        int temp = currentMultiplier * unknown.makeGuess(guess);
        if(temp == 0){
            incorrect.setText("Incorrect!");
        }
        else {
            incorrect.setText("Correct!");
        }
        intScore += temp;
        setScore();
    }
    private void setScore(){
        score.setText("Score: $" + intScore);
    }
    private void reset(){
        wheel.reset();
        phraseIndex = (phraseIndex + 1) % phrasesList.size();
        unknown.reset(phrasesList.get(phraseIndex));
        currentMultiplier = 1;
        currentMultDisplay.setText("Current Multiplier: spin!");
        intScore = 0;
        setScore();
    }
}