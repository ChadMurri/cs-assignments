import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


class CenterCircle extends Pane{
    CenterCircle(int centerX, int centerY){
        Circle back = new Circle(centerX, centerY, 50);
        back.setFill(Color.WHITE);
        back.setStrokeWidth(2);
        back.setStroke(Color.BLACK);
        getChildren().add(back);

        Text spin = new Text();
        spin.setText("Spin!");
        spin.setFont(new Font(30));
        spin.setFill(Color.WHITE);
        spin.setStrokeWidth(2);
        spin.setStroke(Color.BLACK);
        spin.setX(centerX - 30);
        spin.setY(centerY + 10);
        getChildren().add(spin);
    }
}