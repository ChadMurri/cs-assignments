import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;

class TextHidden extends Pane {
    private int x;
    private int y;
    private char[] charList;
    private Text[] letters;
    private ArrayList<Character> alreadyGuessed = new ArrayList<>();
    TextHidden(int x, int y, String phrase){
        this.x = x;
        this.y = y;
        setText(phrase);
    }
    //function sets the underlined "hidden letters" for display like hangman
    private void setText(String next){
        letters = new Text[next.length()];
        charList = next.toCharArray();
        for(int i = 0; i < charList.length; i++){
            char temp = charList[i];
            if(Character.isAlphabetic(temp)){
                temp = " ".toCharArray()[0];
            }
            int spacing = 15;
            Text letter = new Text();
            letter.setText(Character.toString(temp));
            letter.setFont(new Font(15));
            letter.setX(x + i * spacing);
            letter.setY(y);
            letters[i] = letter;
            if(charList[i] != " ".toCharArray()[0]){
                Line undline = new Line(x + i * spacing, y + 5, x + i * spacing + 7, y + 5);
                undline.setStrokeWidth(5);
                getChildren().add(undline);
            }
        }
        for(Text i: letters){
            getChildren().add(i);
        }
    }
    //resets the game
    void reset(String nextPhrase){
        getChildren().remove(0, getChildren().size());
        setText(nextPhrase);
    }
    //checks the guess and updates the underlined thing
    int makeGuess(String guess){
        char temp = guess.toLowerCase().toCharArray()[0];
        int score = 0;
        for(int i = 0; i < charList.length; i++){
            if(temp == Character.toLowerCase(charList[i]) && !alreadyGuessed.contains(temp)){
                letters[i].setText(Character.toString(charList[i]));
                score ++;
            }
        }
        if(score != 0){
            alreadyGuessed.add(temp);
        }
        return score;
    }
}