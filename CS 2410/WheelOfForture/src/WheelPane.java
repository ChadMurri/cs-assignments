import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

class WheelPane extends Pane{
    private int[] points = {450, 650, 350, 700, 600, 1500, 1000, 0, 900, 800, 650, 500,};
    //private int[] test = { 800, 1, 700, 2, 650, 0, 600, 500, 550, 600, 3, 700};
    private double rot = 0;
    private double randomInput;
    private int scoreIndex = 0;
    WheelPane(int radius){
        //sets the wedges
        Paint[] colorWheel = {Color.RED, Color.ORANGE, Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE, Color.WHITE, Color.ORANGE, Color.GREEN, Color.PINK, Color.PURPLE, Color.RED, Color.WHITE, Color.BLUE, Color.LIGHTBLUE, Color.PURPLE, Color.BLACK, Color.PINK, Color.GREEN, Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW};
        int numOfWedges = points.length;
        double rotAngle = (double) 360 / numOfWedges;
        for(int i = 0; i < numOfWedges; i++) {
            Arc wedge = new Arc(radius + 2, radius + 2, radius, radius, i * rotAngle,rotAngle);
            wedge.setType(ArcType.ROUND);
            wedge.setStroke(Color.BLACK);
            wedge.setStrokeWidth(2);
            wedge.setFill(colorWheel[i]);
            getChildren().add(wedge);
        }
        for(int i = 0; i < points.length; i ++) {
            String score;
            int temp = radius - 10;
            char[] wedgeLetters;
            if (points[i] == 0) {
                score = "BANKRUPT";
            } else {
                score = "$" + points[i];
            }
            wedgeLetters = score.toCharArray();
            for (int n = 0; n < score.length(); n++) {
                double angle = - Math.PI * (i * rotAngle + rotAngle / 2) / 180;
                Text text = new Text();
                text.setText(Character.toString(wedgeLetters[n]));
                text.setFont(Font.font("New Times Roman", FontWeight.BOLD, 10));
                text.setFill(Color.BLACK);
                text.setX(Math.cos(angle) * temp + radius);
                text.setY(Math.sin(angle) * temp + radius + 5);
                text.setRotate(-(i * rotAngle + rotAngle / 2) + 90);
                temp -= 10;
                getChildren().add(text);
            }
        }
    }
    void reset(){
        this.setRotate(0);
    }
    int getScoreFromRot(){
        return points[scoreIndex];
    }
    void makeTheWheelFuckingZoopDeWoopMyBOI(){
            randomInput = Math.random() + 1.5;
            setIndex(randomInput, rot);
            Timeline tL = new Timeline();
            tL.getKeyFrames().add(new KeyFrame(Duration.millis(10), t -> spinToWin()));
            tL.setCycleCount(450);
            tL.playFromStart();
    }
    private void setIndex(double temp, double rotTemp){
        while( temp > -2){
            rotTemp = (rotTemp + .25 * Math.exp(temp * 2)) % 360;
            temp -= .01;
        }
        scoreIndex = (int) rotTemp / (360 / points.length);
    }
    private void spinToWin(){
        spinDecay();
        this.setRotate(rot);
    }
    private void spinDecay(){
        if( randomInput > -2){
            rot = (rot + .25 * Math.exp(randomInput * 2)) % 360;
            randomInput -= .01;
        }
    }
}