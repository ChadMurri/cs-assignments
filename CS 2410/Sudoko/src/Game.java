import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;



public class Game extends Application {
    private Board board = new Board("Sudoku.txt");
    private Pane pane = new Pane();
    private boolean sFlag = false;
    private Text isSolved = new Text("Solved");
    //gets a pane ready for when someone wants to display the solution
    private Pane solution = new Pane();
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
        int yOffset = 30;
        //get board
        board.setLayoutY(yOffset);
        pane.getChildren().add(board);
        solution.setLayoutY(yOffset);
        //solve and clear buttons
        Button solve = new Button("Solve");
        solve.setOnAction(e -> solveLogic());
        solve.setLayoutY(280 + yOffset);
        solve.setLayoutX(88);
        pane.getChildren().add(solve);
        Button clear = new Button("Clear");
        clear.setLayoutY(280 + yOffset);
        clear.setLayoutX(140);
        clear.setOnAction(e -> {
                    if (sFlag) {
                        pane.getChildren().removeAll(solution, isSolved);
                        sFlag = false;
                    } else {
                        board.reset();
                    }
                }
        );
        pane.getChildren().add(clear);
        //formats the solved message
        isSolved.setX(150);
        isSolved.setY(20);
        // the grid lines
        Line l1 = new Line(92, 33, 92, 300);
        pane.getChildren().add(l1);
        Line l2 = new Line(182, 33, 182, 300);
        pane.getChildren().add(l2);
        Line l3 = new Line(3, 122, 273, 122);
        pane.getChildren().add(l3);
        Line l4 = new Line(3, 212, 273, 212);
        pane.getChildren().add(l4);
        //Combo box for selecting different board files
        ComboBox<String> menu = new ComboBox<>();
        menu.getItems().add("Sudoku.txt");
        menu.getItems().add("Sudoku1.txt");
        menu.getItems().add("Sudoku2.txt");
        menu.getItems().add("Sudoku3.txt");
        menu.getItems().add("Sudoku4.txt");
        menu.getItems().add("Sudoku5.txt");
        menu.getItems().add("Sudoku6.txt");
        menu.getItems().add("Sudoku7.txt");
        menu.valueProperty().setValue("Sudoku.txt");
        //combo box logic
        menu.setOnAction(e -> {
                if(sFlag){
                    pane.getChildren().removeAll(solution, isSolved);
                    sFlag = false;
                }
                pane.getChildren().remove(board);
                Board newBoard = new Board(menu.valueProperty().getValue());
                newBoard.setLayoutY(yOffset);
                setBoard(newBoard);
                pane.getChildren().add(board);
            }
        );
        menu.setLayoutX(4);
        menu.setLayoutY(4);
        pane.getChildren().add(menu);
        Scene scene = new Scene(pane, 276, 316 + yOffset);
        primaryStage.setTitle("Sudoko"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }
    private void setBoard(Board b){
        board = b;
    }
    private void solveLogic(){
        if(!sFlag){
            solution = board.solutionPane();
            solution.setLayoutY(30);
            pane.getChildren().add(solution);
            sFlag = true;
            if(board.getSolved()){
                isSolved.setText("Solved!");
            }
            else{
                isSolved.setText("Invalid Solution");
            }
            pane.getChildren().add(isSolved);
        }
    }
}