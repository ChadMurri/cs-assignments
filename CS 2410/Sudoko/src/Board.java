import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


class Board extends Pane {
    private TextField[][] boardField = new TextField[9][9];
    private int[][] board = new int[9][9];
    private boolean solved = true;
    //No arg constructor: just makes a blank board
    Board(){
        for(int i = 0; i < 9; i ++){
            for(int n = 0; n < 9; n ++){
                boardField[i][n] = new TextField();
                boardField[i][n].setLayoutX(i * 30 + 4);
                boardField[i][n].setLayoutY(n * 30 + 4);
                boardField[i][n].setPrefWidth(26);
                getChildren().add(boardField[i][n]);
            }
        }
    }
    //takes a file and creates a board with spaces filled in as specified by the file
    Board(String fileName){
        readInGame(fileName);
        for(int i = 0; i < 9; i ++){
            for(int n = 0; n < 9; n ++){
                String num = "";
                if(board[i][n] != 0){
                    num = board[i][n] + "";
                }
                boardField[i][n] = new TextField(num);
                boardField[i][n].setLayoutX(i * 30 + 4);
                boardField[i][n].setLayoutY(n * 30 + 4);
                boardField[i][n].setPrefWidth(26);
                getChildren().add(boardField[i][n]);
            }
        }
    }
    //reset function
    void reset(){
        for(int i = 0; i < 9; i++){
            for(int n = 0; n < 9; n++){
                if(board[i][n] == 0){
                    boardField[i][n].setText("");
                }
                else{
                    boardField[i][n].setText(board[i][n] + "");
                }
            }
        }
    }
    boolean getSolved(){
        return solved;
    }
    Pane solutionPane(){
        //makes two boards for comparison
        int[][] current = new int[9][9];
        int[][] sol = new int[9][9];
        for(int i = 0; i < 9; i++){
            for(int n = 0; n < 9; n++){
                int temp;
                if(boardField[i][n].getText().equals("")){
                    temp = 0;
                }
                else{
                    temp = Integer.parseInt(boardField[i][n].getText());
                }
                current[i][n] = temp;
                sol[i][n] = board[i][n];
            }
        }
        search(sol);
        Pane solution = new Pane();
        int spacing = 30;
        for(int i = 0; i < 9; i++){
            for(int n = 0; n < 9; n++){
                //determines whether a number should be blue, red, or black
                int temp = current[i][n];
                Text s = new Text();
                if(temp == 0){
                    s.setText(Integer.toString(sol[i][n]));
                    s.setFill(Color.BLUE);
                    solved = false;

                }
                else if(temp != sol[i][n]){
                    s.setText(Integer.toString(temp));
                    s.setFill(Color.RED);
                    solved = false;
                }
                else{
                    s.setText(Integer.toString(temp));
                }
                s.setX(i * spacing + 12);
                s.setY(n * spacing + 21);
                solution.getChildren().add(s);
            }
        }
        return solution;
    }
    /** Obtain a list of free cells from the puzzle */
    private int[][] getFreeCellList(int[][] grid) {
        // Determine the number of free cells
        int numberOfFreeCells = 0;
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (grid[i][j] == 0)
                    numberOfFreeCells++;

        // Store free cell positions into freeCellList
        int[][] freeCellList = new int[numberOfFreeCells][2];
        int count = 0;
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (grid[i][j] == 0) {
                    freeCellList[count][0] = i;
                    freeCellList[count++][1] = j;
                }

        return freeCellList;
    }
    /** Search for a solution */
    boolean search(int[][] grid) {
        int[][] freeCellList = getFreeCellList(grid); // Free cells
        if (freeCellList.length == 0)
            return true; // "No free cells");

        int k = 0; // Start from the first free cell
        while (true) {
            int i = freeCellList[k][0];
            int j = freeCellList[k][1];
            if (grid[i][j] == 0)
                grid[i][j] = 1; // Fill the free cell with number 1

            if (isValid(i, j, grid)) {
                if (k + 1 == freeCellList.length) { // No more free cells
                    return true; // A solution is found
                }
                else { // Move to the next free cell
                    k++;
                }
            }
            else if (grid[i][j] < 9) {
                // Fill the free cell with the next possible value
                grid[i][j] = grid[i][j] + 1;
            }
            else { // free cell grid[i][j] is 9, backtrack
                while (grid[i][j] == 9) {
                    if (k == 0) {
                        return false; // No possible value
                    }
                    grid[i][j] = 0; // Reset to free cell
                    k--; // Backtrack to the preceding free cell
                    i = freeCellList[k][0];
                    j = freeCellList[k][1];
                }

                // Fill the free cell with the next possible value,
                // search continues from this free cell at k
                grid[i][j] = grid[i][j] + 1;
            }
        }
    }
    /** Check whether grid[i][j] is valid in the grid */
    private boolean isValid(int i, int j, int[][] grid) {
        // Check whether grid[i][j] is valid at the i's row
        for (int column = 0; column < 9; column++)
            if (column != j && grid[i][column] == grid[i][j])
                return false;

        // Check whether grid[i][j] is valid at the j's column
        for (int row = 0; row < 9; row++)
            if (row != i && grid[row][j] == grid[i][j])
                return false;

        // Check whether grid[i][j] is valid in the 3 by 3 box
        for (int row = (i / 3) * 3; row < (i / 3) * 3 + 3; row++)
            for (int col = (j / 3) * 3; col < (j / 3) * 3 + 3; col++)
                if (row != i && col != j && grid[row][col] == grid[i][j])
                    return false;

        return true; // The current value at grid[i][j] is valid
    }
    //Reads in a file from the data directory and sets the current int list to the files board
    private void readInGame(String fileName){
        try {
            BufferedReader read = new BufferedReader(new FileReader(new File("data/" + fileName)));
            String text;
            String[] strNum;
            int count = 0;
            while ((text = read.readLine()) != null) {
                strNum = text.split(" ");
                for(int i = 0; i < 9; i++){
                    board[count][i] = Integer.parseInt(strNum[i]);
                }
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}