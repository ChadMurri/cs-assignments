

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.scene.shape.Line;



public class HangMan extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
        // Create a scene and place it in the stage
        VBox pane = new VBox();
        LinePane hangMan = new LinePane();
        pane.getChildren().add(hangMan);

        TextField guessField = new TextField();
        Label guess = new Label("Guess here: ", guessField);
        guess.setContentDisplay(ContentDisplay.RIGHT);
        pane.getChildren().add(guess);

        Scene scene = new Scene(pane, 500, 500);
        primaryStage.setTitle("Hang Man"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }
}
class Game{
    private StringBuilder display = new StringBuilder();
    private String phrase;
    Game(String phrase){
      this.phrase = phrase;
      for(int i = 0; i < phrase.length(); i ++){
          display.append(" ");
      }
    }
    StringBuilder getDisplay(){
        return display;
    }
    boolean checkGuess(String guess){
        if(guess.length() == 1){
            char guessChar = guess.toLowerCase().toCharArray()[0];
            char[] temp = phrase.toLowerCase().toCharArray();
            for(char i: temp){
                if(guessChar == i){
                    display.replace(i, i, Character.toString(guessChar));
                }
            }
        }
        return true;
    }
}
class LinePane extends Pane {
    LinePane() {
        int gallowsHeight = 400;
        int beamWidth = 100;
        int mainBeamX = 100;
        Line line1 = new Line(mainBeamX, 100, 100, gallowsHeight);
        line1.setStrokeWidth(5);
        getChildren().add(line1);

        Line line2 = new Line(mainBeamX, 100, 200, 100);
        line2.setStrokeWidth(5);
        getChildren().add(line2);

        Line line3 = new Line(mainBeamX + beamWidth, 100, mainBeamX + beamWidth, 150);
        line3.setStrokeWidth(5);
        getChildren().add(line3);

        Arc arc1 = new Arc(100, gallowsHeight + 25, 50, 25, 0, 180);
        arc1.setFill(Color.WHITE);
        arc1.setStroke(Color.BLACK);
        arc1.setStrokeWidth(5);
        getChildren().add(arc1);
    }
}

