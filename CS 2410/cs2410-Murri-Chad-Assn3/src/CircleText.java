import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.lang.Math;

public class CircleText extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
        // Create a scene and place it in the stage
        String msg = "WElCOME TO JAVA ";
        int centerX = 200;
        int centerY = 200;
        int radius = 125;
        Pane pane = new Pane();
        for (int i = 0; i < msg.length(); i ++){
            Text temp = new Text();
            temp.setText("" + msg.charAt(i));
            temp.setRotate(90 + 360/(double)msg.length() * i);
            temp.setX(centerX + radius * Math.cos((Math.PI /((double)msg.length()/2)) * i));
            temp.setY(centerY + radius * Math.sin((Math.PI /((double)msg.length()/2)) * i));
            pane.getChildren().add(temp);
        }
        Scene scene = new Scene(pane, 400, 400);
        primaryStage.setTitle("Circle Text"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }
}
