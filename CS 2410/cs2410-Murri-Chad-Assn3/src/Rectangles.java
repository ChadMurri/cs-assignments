import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.Scanner;

public class Rectangles extends Application {
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
        // Create a scene and place it in the stage
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the height and then width of the first rectangle.");
        int height1 = input.nextInt();
        int width1 = input.nextInt();
        System.out.println("Please enter the height and then width of the second rectangle.");
        int height2 = input.nextInt();
        int width2 = input.nextInt();
        System.out.println("Now enter the X and Y coordinates of the first one.");
        int x1 = input.nextInt();
        int y1 = input.nextInt();
        System.out.println("Now enter the X and Y coordinates of the second one.");
        int x2 = input.nextInt();
        int y2 = input.nextInt();

        Text msg = new Text();
        boolean inside = x1 < x2 && (x1 + width1) > (x2 + width2) && y1 < y2 && (y1 + height1) > (y2 + height2) || x2 < x1 && (x2 + width2) > (x1 + width1) && y2 < y1 && (y2 + height2) > (y1 + height1);
        if (inside){
            msg.setText("One is contained in the other");
        }
        else if((x1 + width1) - x2 >= 0 && (x2 + width2) - x1 >= 0 || (y1 + height1) - y2 >= 0 && (y2 + height2) - y1 >= 0){
            msg.setText("One intersects the other");
        }
        else{
            msg.setText("The shapes are entirely separate");
        }
        msg.setX(x1 + (double)width1 / 2);
        msg.setY(y1 + height1 + 100);

        Pane pane = new Pane();

        pane.getChildren().add(msg);

        Rectangle rec1 = new Rectangle();
        rec1.setHeight(height1);
        rec1.setWidth(width1);
        rec1.setX(x1);
        rec1.setY(y1);
        rec1.setStroke(Color.BLACK);
        rec1.setFill(Color.TRANSPARENT);
        pane.getChildren().add(rec1);

        Rectangle rec2 = new Rectangle();
        rec2.setHeight(height2);
        rec2.setWidth(width2);
        rec2.setX(x2);
        rec2.setY(y2);
        rec2.setStroke(Color.BLACK);
        rec2.setFill(Color.TRANSPARENT);
        pane.getChildren().add(rec2);

        Scene scene = new Scene(pane, 400, 400);
        primaryStage.setTitle("Rectangles"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display
        // the stage
    }
}
