/**
 * 1. Run App,
 * 2. Demo Fonts, sizes, and colors
 * 3. goto ShowImage
 */

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class HelloWorld extends Application {
    @Override
    public void start(Stage stage) {
        //Creating a Text object
        Image img = new Image("https://www.3pillarglobal.com/wp-content/uploads/2016/03/java8_600x600-300x300.png");
        ImageView thisOne = new ImageView(img);
        Text text = new Text();
        Text text2 = new Text();
        //Setting the text to be added.
        text.setText("Hello");
        text2.setText("World");
        text.setFill(Color.BLACK);
        text2.setFill(Color.RED);
        text.setX(50);
        text.setY(50);
        text2.setX(100);
        text2.setY(50);
        //Setting font to the text
        //text.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 20));
        //text.setFill(Color.AQUA);
        //Creating a Group object
        Pane pane = new Pane();
        pane.getChildren().add(text);
        pane.getChildren().add(text2);
        pane.getChildren().add(thisOne);
        //Creating a scene object
        Scene scene = new Scene(pane, 600, 300);

        //Setting title to the Stage
        stage.setTitle("Hello_World");

        //Adding scene to the stage
        stage.setScene(scene);

        //Displaying the contents of the stage
        stage.show();
    }
    public static void main(String args[]){
        launch(args);
    }
}