class Cube extends Shape{
    private double side;
    Cube(double side){
        this.side = side;
    }
    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    double getArea(){
        return side * side * 6;
    }
}