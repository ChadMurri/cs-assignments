class Triangle extends Shape{
    private double side;
    Triangle(double side){
        this.side = side;
    }
    public double getSide() {
        return side;
    }
    public void setSide(double side) {
        this.side = side;
    }
    double getArea(){
        return .5 * side * side;
    }
}