class Pyramid extends Shape{
    private double side;
    Pyramid(double side){
        this.side = side;
    }
    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    double getArea(){
        return .5 * side * side * 4;
    }
}