class driver{
    private static void printShape(Shape s){
        System.out.println("This shape has the area: " + s.getArea());
    }
    public static void main(String[] args) {
        Shape[] shapesList = new Shape[4];
        shapesList[0] = new Square(4);
        shapesList[1] = new Cube(5);
        shapesList[2] = new Triangle(7);
        shapesList[3] = new Pyramid(3);

        for(Shape i : shapesList){
            System.out.println("This shape has area: " + i.getArea());
        }
    }
}