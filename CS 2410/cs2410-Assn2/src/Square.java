class Square extends Shape{
    private double side;
    Square(double side){
        this.side = side;
    }
    public double getSide() {
        return side;
    }
    public void setSide(double side) {
        this.side = side;
    }
    double getArea(){
        return side*side;
    }
}