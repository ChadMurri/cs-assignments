package week7.thu;

//1. Discuss Dragged
//Review Book page 602 show Methods
//Review page 589 Mouse Event Registration methods
// - add to pane setOnMouseClicked(), setOnMousePressed(), setOnMouseReleased()
// - show dragged output
//Review Lambdas
//GOTO

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;


import java.util.ArrayList;
import java.util.Timer;

public class ConnectFour extends Application {
    private int[][] shadowBoard = new int[6][7];
    private Circle[][] spaces = new Circle[6][7];
    private boolean flag = true;
    private Text winText = new Text();
    @Override // Override the start method in the Application class

    public void start(Stage primaryStage) {
        // Create a pane and set its properties
        Pane pane = new Pane();
        // sets the background of the pane to blue
        pane.setBackground(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
        // setting white circles onto the pane
        for(int i = 0; i < 6; i ++){
            for(int n = 0; n < 7; n ++){
                spaces[i][n] = new Circle();
                spaces[i][n].setRadius(40);
                spaces[i][n].setCenterX(50 + 90*n);
                spaces[i][n].setCenterY(50 + 90*i);
                spaces[i][n].setFill(Color.WHITE);
                spaces[i][n].setStroke(Color.BLACK);
                spaces[i][n].setStrokeWidth(3);
                pane.getChildren().add(spaces[i][n]);
            }
        }
        //sets the click handling for pane
        pane.setOnMouseClicked(e -> addPiece(e.getX(),e.getY()));
        //reset button
        Button resetButton = new Button();
        resetButton.setLayoutX(295);
        resetButton.setLayoutY(550);
        resetButton.setText("Reset Game");
        resetButton.setOnAction(e -> reset());
        pane.getChildren().add(resetButton);
        //initializes the board the logic is ran on
        for (int i = 0; i < 6; i ++){
            for (int n = 0; n < 7; n ++){
                shadowBoard[i][n] = 0;
            }
        }
        //Text presented when won
        winText.setX(50);
        winText.setY(275);
        winText.setFont(new Font(100));
        winText.setStroke(Color.BLACK);
        winText.setStrokeWidth(3);
        winText.setFill(Color.TRANSPARENT);
        winText.setTextAlignment(TextAlignment.JUSTIFY);
        pane.getChildren().add(winText);
        // Create a scene and place it in the stage
        Scene scene = new Scene(pane, 7 * 90 + 10, 6 * 90 + 10 + 40);
        primaryStage.setTitle("Connect Four"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }
    //handles the conversion from pixels to x and y coordinates and then changes the color of a white piece to alternating colors red and yellow.
    private void addPiece(double x, double y){
        int xCoord = (int)(x/90);
        int yCoord = (int)(y/90);
        if(checkInBound(xCoord, yCoord)) {
            while (yCoord < 5 && shadowBoard[yCoord + 1][xCoord] == 0) {
                yCoord++;
            }
            if (flag && shadowBoard[yCoord][xCoord] != 2) {
                spaces[yCoord][xCoord].setFill(Color.RED);
                shadowBoard[yCoord][xCoord] = 1;
                flag = false;
            } else if (!flag && shadowBoard[yCoord][xCoord] != 1) {
                spaces[yCoord][xCoord].setFill(Color.YELLOW);
                shadowBoard[yCoord][xCoord] = 2;
                flag = true;
            }
            //calls the win() which checks if a piece has won the game
            win(xCoord, yCoord);
        }
    }
    //checks if a game has been won
    private void win(int x, int y){
        int id = shadowBoard[y][x];
        Circle curr = spaces[y][x];
        ArrayList<Circle> down = new ArrayList<>();
        down.add(curr);
        down.addAll(win(x, y + 1, 0, 1, id, new ArrayList<>()));
        ArrayList<Circle> horizontal = new ArrayList<>();
        horizontal.add(curr);
        horizontal.addAll(win(x + 1, y, 1, 0, id, new ArrayList<>()));
        horizontal.addAll(win(x - 1, y, -1, 0, id, new ArrayList<>()));
        ArrayList<Circle> upRight = new ArrayList<>();
        upRight.add(curr);
        upRight.addAll(win(x + 1, y + 1, 1, 1, id, new ArrayList<>()));
        upRight.addAll(win(x - 1, y - 1, -1, -1, id, new ArrayList<>()));
        ArrayList<Circle> upLeft = new ArrayList<>();
        upLeft.add(curr);
        upLeft.addAll(win(x + 1, y - 1, 1, -1, id, new ArrayList<>()));
        upLeft.addAll(win(x - 1, y + 1, -1, 1, id, new ArrayList<>()));
        checkSize(down);
        checkSize(horizontal);
        checkSize(upLeft);
        checkSize(upRight);
    }
    private ArrayList<Circle> win(int x, int y, int stepX, int stepY, int id, ArrayList<Circle> list){
        if(!checkInBound(x, y) || shadowBoard[y][x] != id){
            return list;
        }
        list.add(spaces[y][x]);
        return win(x + stepX, y + stepY, stepX, stepY, id, list);
    }
    private void checkSize( ArrayList<Circle> list){
        if( list.size() >= 4){
            makeGlow(list, Color.rgb((int)(Math.random() * 255), (int)(Math.random() * 255), (int)(Math.random() * 255)), list.get(0).getFill());
        }
    }
    private void makeGlow(ArrayList<Circle> winning, Paint flash, Paint original){
        Paint color = winning.get(0).getFill();
        if( color == Color.RED){
            winText.setX(50);
            winText.setFill(color);
            winText.setText("RED WINS!!!");
        }
        else{
            winText.setX(0);
            winText.setFill(color);
            winText.setText("YELLOW WINS!!!");
        }
        for(int i = 0; i < 4; i++){
            Timeline tL1 = new Timeline();
            Timeline tL2 = new Timeline();
            Circle temp = winning.get(i);
            tL1.getKeyFrames().add(new KeyFrame(Duration.millis(500), e -> {temp.setFill(Color.rgb((int)(Math.random() * 255), (int)(Math.random() * 255), (int)(Math.random() * 255))); }));
            tL2.getKeyFrames().add(new KeyFrame(Duration.millis(500), e -> {temp.setFill(original); }));
            tL1.setDelay(Duration.millis(0));
            tL2.setDelay(Duration.millis(250));
            tL1.setCycleCount(6);
            tL2.setCycleCount(5);
            tL1.play();
            tL2.play();

        }
    }

    private boolean checkInBound(int x, int y){
        return x >= 0 && x < 7 && y >= 0 && y < 6;
    }
    //reset function
    private void reset(){
        winText.setFill(Color.TRANSPARENT);
        winText.setText("");
        System.out.println("RESET");
        for(int i = 0; i < 6; i ++){
            for(int n = 0; n < 7; n ++){
                spaces[i][n].setFill(Color.WHITE);
                shadowBoard[i][n] = 0;
            }
        }
    }
    /**
     * The main method is only needed for the IDE with limited
     * JavaFX support. Not needed for running from the command line.
     */
    public static void main(String[] args) {
        launch(args);
    }
}