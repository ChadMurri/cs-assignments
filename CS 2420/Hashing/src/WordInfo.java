class WordInfo{
    private String word;
    private int uses;
    WordInfo(String word){
        this.word = word;
        uses = 0;
    }
    //returns the word
    String getWord(){
        return word;
    }
    void addUse(){
        uses += 1;
    }
    int getUses(){
        return uses;
    }
    public int hashCode(){
        return word.hashCode();
    }
//    public boolean equals(WordInfo t){
//        return word.equals(t.getWord());
//    }
    @Override
    public boolean equals(Object w2){
        if (w2==this){
            return true;
        }
        if (!(w2 instanceof WordInfo)){
            return false;
        }
        WordInfo w= (WordInfo)w2;
        return (this.word.compareTo(w.word) ==0);
    }
    public String toString(){
        return String.format("%-12s Uses: %d", word, uses + 1);
    }
}