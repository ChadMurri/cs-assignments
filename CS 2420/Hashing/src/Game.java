
import java.io.*;
import java.util.ArrayList;

public class Game {
    private Game(){
        H = new QuadraticProbingHashTable<WordInfo>();
        score = 0;
    }

    private int computeScore(WordInfo wi){
        int[] scoreList = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};
        int lengthScore;
        int uniqueScore;
        int score = 0;
        String word = wi.getWord();
        int uses = wi.getUses();
        //calc the letter score
        char[] letters = word.toCharArray();
        for( char i: letters){
            score += scoreList[(int)i - (int)'a'];
        }
        //calc the length score
        if(word.length() > 7){
            lengthScore = 6;
        }
        else if(word.length() < 3){
            lengthScore = 0;
        }
        else{
            lengthScore = word.length() - 2;
        }
        //calc the score from being unique
        if(uses > 15){
            uniqueScore = 1;
        }
        else if(uses > 10){
            uniqueScore = 2;
        }
        else if(uses > 5){
            uniqueScore = 3;
        }
        else if(uses > 0){
            uniqueScore = 4;
        }
        else{
            uniqueScore = 5;
        }
        return score * lengthScore * uniqueScore;
    }

    private void playGame(String filename){
        name = filename;
        ArrayList<String> dupes = new ArrayList<>();
        try {
            BufferedReader read = new BufferedReader(new FileReader(new File(filename)));
            String text;
            while((text = read.readLine()) != null){
                WordInfo temp = new WordInfo(text);
                if(dupes.contains(text)){
                    WordInfo tmp = H.find(temp);
                    tmp.addUse();
                    score += computeScore(tmp);
                }
                else{
                    dupes.add(text);
                    H.insert(temp);
                    score += computeScore(temp);
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public String toString() {
        int LIMIT = 20;
        return name+ "\n"+ H.toString(LIMIT) + printScore() + "\n";
    }
    private String printScore(){
        return "Score: " + score + " ";
    }

    private String name;
    private QuadraticProbingHashTable<WordInfo> H;
    private int score;


    public static void main( String [ ] args ) {
        Game g0 = new Game(  );
        g0.playGame("game0.txt" );
        System.out.println(g0);
        g0.printScore();
//        Game g1 = new Game(  );
//        g1.playGame("game1.txt" );
//        System.out.println(g1);
        Game g2 = new Game(  );
        g2.playGame("game2.txt" );
        System.out.println(g2);
        g2.printScore();
        Game g3 = new Game(  );
        g3.playGame("game3.txt" );
        System.out.println(g3);
        Game g4 = new Game(  );
        g4.playGame("game4.txt" );
        System.out.println(g4);
        g4.printScore();
    }
}

