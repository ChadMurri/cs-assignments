import java.lang.Math;
import java.util.ArrayList;

class SliderPuzzle implements Comparable<SliderPuzzle>{
    //The Class Data for the size, last move taken, the coordinates of the blank space, and the board (a two dimensional array of ints).
    private int size = 3;
//    private Integer manhattanDist = 0;
    private Integer movesTaken = 0;
    private Integer cost = 0;
    private String lastMove;
    private int blankRow;
    private int blankCol;
    private int[][] board = new int[size][size];
    //Keeps track of moves so a solution can be presented when one is found
    private String moveList = "";
    //default constructor
    SliderPuzzle(){
        initialize();
        blankRow = size - 1;
        blankCol = size - 1;
        lastMove = "NA";
    }
    //clone constructor
    SliderPuzzle(SliderPuzzle original){
        for(int i = 0; i < original.getBoard().length; i ++){
            board[i] = original.getBoard()[i].clone();
        }
        lastMove = original.getLastMove();
        blankRow = original.getBlankRow();
        blankCol = original.getBlankCol();
        size = getSize();
        moveList = original.getMoveList();
        movesTaken = original.movesTaken + (Integer)1;
    }
    //created a prejumbled puzzle
    SliderPuzzle(int timesJumbled){
        initialize();
        blankRow = size - 1;
        blankCol = size - 1;
        lastMove = "NA";
        jumble(timesJumbled);
    }
    //internal excessor methods used for cloning the class
    private int[][] getBoard(){
        return board;
    }
    private int getBlankRow(){
        return blankRow;
    }
    private int getBlankCol(){
        return blankCol;
    }
    private String getLastMove(){
        return lastMove;
    }
    private int getSize(){
        return size;
    }
    //gets the move list so it can be printed
    private String getMoveList(){
        return moveList;
    }
    //adds moves to the move list
    private void addMove(String direction){
        moveList += direction;
    }
    //resets the last move so that solving the puzzle doesn't immediately trip it
    private void resetLastMove(){
        lastMove = "NA";
    }
    //Checks if a board is Solved by iterating through the 2d array
    private boolean isSolved(){
        int count = 1;
        for(int i = 0; i < size; i ++){
            for(int n = 0; n < size; n ++){
                if(count == size * size){
                    return true;
                }
                if(board[i][n] != count){
                    return false;
                }
                count ++;
            }
        }
        return true;
    }
    //Checks if a chosen move is valid based on the last move taken and the the boundaries of the board.
    private boolean isMoveValid(String direction){
        if(direction.compareTo("D") == 0 && lastMove.compareTo("U") != 0 && blankRow != 0){
            return true;
        }
        if(direction.compareTo("L") == 0 && lastMove.compareTo("R") != 0 && blankCol != size - 1){
            return true;
        }
        if(direction.compareTo("U") == 0 && lastMove.compareTo("D") != 0 && blankRow != size - 1){
            return true;
        }
        return (direction.compareTo("R") == 0 && lastMove.compareTo("L") != 0 && blankCol != 0);
    }
    //moves a number into the blank. First the move is checked out to see if it's valid, and if it is the move executes
    //by replacing the blank with the slid tile and then setting the tiles previous spot to blank
    void move(String directions){
        String direction;
        for(int i = 0; i < directions.length(); i++) {
            direction = "" + directions.charAt(i);
            if(!isMoveValid(direction)){
                return;
            }
            if (direction.compareTo("D") == 0) {
                board[blankRow][blankCol] = board[blankRow - 1][blankCol];
                board[blankRow - 1][blankCol] = 0;
                blankRow--;
                lastMove = "D";
            }
            if (direction.compareTo("L") == 0) {
                board[blankRow][blankCol] = board[blankRow][blankCol + 1];
                board[blankRow][blankCol + 1] = 0;
                blankCol++;
                lastMove = "L";
            }
            if (direction.compareTo("U") == 0) {
                board[blankRow][blankCol] = board[blankRow + 1][blankCol];
                board[blankRow + 1][blankCol] = 0;
                blankRow++;
                lastMove = "U";
            }
            if (direction.compareTo("R") == 0) {
                board[blankRow][blankCol] = board[blankRow][blankCol - 1];
                board[blankRow][blankCol - 1] = 0;
                blankCol--;
                lastMove = "R";
            }
        }
//        manhattanDist = calcManDist();
    }
    //fills the board in in order
    private void initialize(){
        int count = 1;
        for(int i = 0; i < size; i++){
            for(int n = 0; n < size; n++){
                if(count == size * size){
                    board[i][n] = 0;
                }
                else {
                    board[i][n] = count;
                }
                count++;
            }
        }
    }
    //outputs the current state of the board
    void printBoard(){
        for(int i = 0; i < size; i++){
            for(int n = 0; n < size; n++){
                System.out.printf("%-3d", board[i][n]);
            }
            System.out.println();
        }
    }
    //takes an int and then randomly scrambles the puzzle using valid moves (so it always moves the inputted amount)
    private void jumble(int timesJumbled){
        int randInput;
        int count = 0;
        String[] moves = {"U", "D", "L", "R"};
        while(count < timesJumbled){
            randInput = (int)(4 * java.lang.Math.random());
            if(isMoveValid(moves[randInput])){
                move(moves[randInput]);
                count++;
            }
        }
    }
    private int hammingDist(){
        int count = 1;
        int dist = 9;
        for (int i = 0; i < size; i++){
            for (int n = 0; n < size; n ++){
                if (board[i][n] == count || count == 9 && board[i][n] == 0){
                    dist --;
                }
                count ++;
            }
        }
        return dist;
    }
    private int calcIndivManDist(int x, int y, int actX, int actY){
        return Math.abs(x - actX + y - actY);
    }
    //Calculates manhattan distance
//    int calcManDist(){
//        int dist = 0;
//        for (int i = 0; i < size; i ++){
//            for (int n = 0; n < size; n ++){
//                int element = board[i][n];
//                if (element == 1){
//                    dist += i + n;
//                }
//                else if (element == 2){
//                    dist += calcIndivManDist(i,n,1,0);
//                }
//                else if (element == 3){
//                    dist += calcIndivManDist(i,n, 2, 0);
//                }
//                else if (element == 4){
//                    dist += calcIndivManDist(i,n,0,1);
//                }
//                else if (element == 5){
//                    dist += calcIndivManDist(i,n, 1,1);
//                }
//                else if (element == 6){
//                    dist += calcIndivManDist(i,n,2,1);
//                }
//                else if (element == 7){
//                    dist += calcIndivManDist(i,n,0,2);
//                }
//                else if (element == 8){
//                    dist += calcIndivManDist(i,n,1,2);
//                }
//            }
//        }
//        return dist;
//    }
    //calculates the "cost" by add the distance to
    private void calcCost(){
        cost = movesTaken + hammingDist();
    }
    //overrides the compareTo method to use a manhattan distance to differentiate boards for the avl tree
    public int compareTo(SliderPuzzle t){
        calcCost();
        t.calcCost();
        return cost.compareTo(t.cost);
    }
    //I simply replaced all references to the old queue to the avl tree
    void aStarsolve(SliderPuzzle board) {
        //data and Tree are set up
        int boardsTakenOffTheTree = 0;
        AVLTree<SliderPuzzle> solutionTree = new AVLTree<>();
        board.resetLastMove();
        solutionTree.insert(board);
        //loops until the state of the board is considered solved
        while (true) {
            //the first board on the on the list is accessed and checked if it's in the solved state
            SliderPuzzle boardState = solutionTree.findMin();
            solutionTree.deleteMin();
            boardsTakenOffTheTree ++;
            if (boardState.isSolved()) {
                //Stats and solution are printed out
                showMeYourMoves(boardState.getMoveList(),board);
                System.out.println((solutionTree.countNodes() + boardsTakenOffTheTree) + " boards were placed in the queue before the puzzle was solved!");
                System.out.println(boardsTakenOffTheTree + " boards were taken off the queue before the solution was found!");
                return;
            }
            //each move is checked to see if it is valid and if it is a copy of the current "min" is created and has the move applied
            if (boardState.isMoveValid("U")) {
                SliderPuzzle up = new SliderPuzzle(boardState);
                up.move("U");
                up.addMove("U");
                solutionTree.insert(up);
            }
            if (boardState.isMoveValid("R")) {
                SliderPuzzle right = new SliderPuzzle(boardState);
                right.move("R");
                right.addMove("R");
                solutionTree.insert(right);
            }
            if (boardState.isMoveValid("D")) {
                SliderPuzzle down = new SliderPuzzle(boardState);
                down.move("D");
                down.addMove("D");
                solutionTree.insert(down);
            }
            if (boardState.isMoveValid("L")) {
                SliderPuzzle left = new SliderPuzzle(boardState);
                left.move("L");
                left.addMove("L");
                solutionTree.insert(left);
            }
        }
    }
    //my old brute force queue
    void bruteForceSolver(SliderPuzzle board) {
        //data and queue list are set up
        int boardsTakenOffTheQueue = 0;
        ArrayList<SliderPuzzle> solutionQueue = new ArrayList<>();
        board.resetLastMove();
        solutionQueue.add(board);
        //loops until the state of the board is considered solved
        while (true) {
            //the first board on the on the list is accessed and checked if it's in the solved state
            SliderPuzzle boardState = solutionQueue.get(0);
            if (boardState.isSolved()) {
                //Stats and solution are printed out
                showMeYourMoves(boardState.getMoveList(),board);
                System.out.println((solutionQueue.size() + boardsTakenOffTheQueue) + " boards were placed in the queue before the puzzle was solved!");
                System.out.println(boardsTakenOffTheQueue + " boards were taken off the queue before the solution was found!");
                return;
            }
            //each move is checked to see if it is valid and if it is a copy of the current first in line is created and has the move applied
            if (boardState.isMoveValid("U")) {
                SliderPuzzle up = new SliderPuzzle(boardState);
                up.move("U");
                up.addMove("U");
                solutionQueue.add(up);
            }
            if (boardState.isMoveValid("R")) {
                SliderPuzzle right = new SliderPuzzle(boardState);
                right.move("R");
                right.addMove("R");
                solutionQueue.add(right);
            }
            if (boardState.isMoveValid("D")) {
                SliderPuzzle down = new SliderPuzzle(boardState);
                down.move("D");
                down.addMove("D");
                solutionQueue.add(down);
            }
            if (boardState.isMoveValid("L")) {
                SliderPuzzle left = new SliderPuzzle(boardState);
                left.move("L");
                left.addMove("L");
                solutionQueue.add(left);
            }
            //the current first in line is removed after all one move board states are added
            solutionQueue.remove(0);
            boardsTakenOffTheQueue++;
        }
    }
    //this function is given the unsolved board and has the move list generated by the solver applied to it. It prints out each board state
    //and it's associated move
    private static void showMeYourMoves(String moves, SliderPuzzle board){
        System.out.println("Unsolved state:");
        board.printBoard();
        System.out.println("Solution: " + moves);
        board.resetLastMove();
        for(int i = 0; i < moves.length(); i++){
            String direction = "" +moves.charAt(i);
            board.move(direction);
            System.out.println("Board after move: " + direction);
            board.printBoard();
        }
        System.out.println("Number of moves till solved: " + moves.length());
    }
}
