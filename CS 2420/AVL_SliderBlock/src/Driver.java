
public class Driver{

    //simple main function for demonstrating the SliderPuzzle class and the solver
    public static void main(String[] args) {
        SliderPuzzle boardBasic = new SliderPuzzle();
        SliderPuzzle boardJumbled = new SliderPuzzle(31);
        SliderPuzzle boardJumbled2 = new SliderPuzzle(boardJumbled);
        System.out.println("Initial board is created with no moves applied:");
        boardBasic.printBoard();
        System.out.println("board can be moved around manually using the class function move, or a class function jumble can take a int and take that many random moves: ");
        boardBasic.move("DRRDLL");
        SliderPuzzle boardBasic2 = new SliderPuzzle(boardBasic);
        boardBasic.printBoard();
        System.out.println("Now a prejumbled board can also be made by calling the SliderPuzzle constructor with an single int argument to signify the number of moves:");
        boardJumbled.printBoard();
        System.out.println("Then the solver is applied to each board and then the solution is printed out after it's found:");
        System.out.println("First the brute force solver is used:");
        boardBasic2.bruteForceSolver(boardBasic2);
        boardJumbled2.bruteForceSolver(boardJumbled2);
        System.out.println("Then the A* solver:");
        boardBasic.aStarsolve(boardBasic);
        boardJumbled.aStarsolve(boardJumbled);
    }
}