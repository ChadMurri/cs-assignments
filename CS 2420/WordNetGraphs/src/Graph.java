import java.io.File;
import java.nio.charset.IllegalCharsetNameException;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Arrays;

public class Graph {
    private int numVertex;  // Number of vertices in the graph.
    private GraphNode[] G;  // Adjacency list for graph.
    private String graphName;  //The file from which the graph was created.


    private Graph() {
        this.numVertex = 0;
        this.graphName = "";
    }

    public Graph(int numVertex) {
        this.numVertex = numVertex;
        G = new GraphNode[numVertex];
        for (int i = 0; i < numVertex; i++) {
            G[i] = new GraphNode( i );
        }
    }

    public boolean addEdge(int source, int destination) {
        if (source < 0 || source >= numVertex) return false;
        if (destination < 0 || destination >= numVertex) return false;
        //add edge
        G[source].addEdge( source, destination );
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append( "The Graph " + graphName + " \n" );

        for (int i = 0; i < numVertex; i++) {
            sb.append( G[i].toString() );
        }
        return sb.toString();
    }

    private void makeGraph(String filename) {
        try {
            graphName = filename;
            Scanner reader = new Scanner( new File( filename ) );
            System.out.println( "\n" + filename );
            numVertex = reader.nextInt();
            G = new GraphNode[numVertex];
            for (int i = 0; i < numVertex; i++) {
                G[i] = new GraphNode( i );
            }
            while (reader.hasNextInt()) {
                int v1 = reader.nextInt();
                int v2 = reader.nextInt();
                G[v1].addEdge( v1, v2 );
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void clearAllPred() {
        for (int i = 0; i < numVertex; i++) {
            G[i].p1.clear();
            G[i].p2.clear();
        }
    }


    private String reportPath(int v1, int v2, int anc) {
        String path = "" + anc;
        int temp = anc;
        while(temp != v1){
            temp = G[temp].p1.pred;
            path = temp + " -> " + path;
        }
        temp = anc;
        while(temp != v2){
            temp = G[temp].p2.pred;
            path = path + " -> " + temp;
        }
        return path;
    }

    /**
     * Computes the least common ancestor of v1 and v2, prints the length of the path, the ancestor, and the path itself.
     *
     * @param v1: first vertex
     * @param v2: second vertex
     * @return returns the length of the shortest ancestral path.
     */
    private void lca(int v1, int v2) {
        // Compute lca
        PathInfo best = new PathInfo();
        prim(v1);
        int anc = prim2(v2);
        int dist = G[anc].p1.dist + G[anc].p2.dist;
        best.set(anc, dist);
        System.out.println( graphName + " Best lca " + v1 + " " + v2 + " Distance: " + best.dist + " Ancestor " + best.pred + " Path: " + reportPath(v1, v2, anc));
        clearAllPred();
    }
    //basically goes up tagging the p1's of nodes until it hits root
    private void prim(int v){
        int dist = 0;
        ArrayList<Integer> queue = new ArrayList<>();
        queue.add(v);
        G[v].p1.set(v, 0);
        while(!queue.isEmpty()){
            queue.remove(0);
            dist ++;
            //goes through a nodes predecessors and then adds them to the queue
            if(!G[v].succ.isEmpty()) {
                for (EdgeInfo i : G[v].succ) {
                    if (G[i.to].p1.dist > dist) {
                        queue.add(i.to);
                        G[i.to].p1.set(v, dist);
                    }
                }
            }
            if(!queue.isEmpty()) {
                v = queue.get(0);
            }
        }
    }
    //goes up the tree until it hits a node tagged by the first prim and since it's breadth that path will be the shortest
    private int prim2(int v){
        int dist = 0;
        ArrayList<Integer> queue = new ArrayList<>();
        queue.add(v);
        G[v].p2.set(v, 0);
        //returns v if it's the best ancestor (i.e. it's an ancestor of the other node
        if(G[v].p1.pred != -1){
            return v;
        }
        while(!queue.isEmpty()){
            queue.remove(0);
            dist ++;
            //goes through a nodes predecessors and then adds them to the queue
            if(!G[v].succ.isEmpty()) {
                for (EdgeInfo i : G[v].succ) {
                    if(G[i.to].p1.pred != -1){
                        G[i.to].p2.set(v, dist);
                        return i.to;
                    }
                    else if (G[i.to].p2.dist > dist) {
                        queue.add(i.to);
                        G[i.to].p2.set(v, dist);
                    }
                }
            }
            if(!queue.isEmpty()) {
                v = queue.get(0);
            }
        }
        return 0;
    }
    private void outcast(int[] v) {
        int outcast = -1;
        int bigBoi = -1;
        for(int i: v){
            int sum = sumDist(i);
            if(sum > bigBoi){
                bigBoi = sum;
                outcast = i;
            }
        }
        System.out.println( "The outcast of " + Arrays.toString( v ) + " is " + outcast );
    }
    private int sumDist(int v){
        int sum = 0;
        for(int i = 0; i < G.length; i++){
            if(v != i){
                prim(v);
                int anc = prim2(i);
                sum += G[anc].p1.dist + G[anc].p2.dist;
                clearAllPred();
            }
        }
        return sum;
    }
    public static void main(String[] args) {
        Graph graph1 = new Graph();
        graph1.makeGraph( "digraph1.txt" );
        //System.out.println(graph.toString());
        int[] set1 = {7, 10, 2};
        int[] set2 = {7, 17, 5, 11, 4, 23};
        int[] set3 = {10, 17, 13};

        graph1.lca( 3, 7 );
        graph1.lca( 5, 6 );
        graph1.lca( 9, 1 );
        graph1.outcast( set1 );

        Graph graph2 = new Graph();
        graph2.makeGraph( "digraph2.txt" );
        //System.out.println(graph2.toString());
        graph2.lca( 3, 24 );

        graph2.outcast( set3 );
        graph2.outcast( set2 );
        graph2.outcast( set1 );
    }
}