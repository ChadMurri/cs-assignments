class LeftistHeap <E extends Comparable<E>>{
    private Node<E> root;
    LeftistHeap(){
        root = null;
    }
    private static class Node<E>{
        private E item;
        private Node<E> left;
        private Node<E> right;
        private int nPL;
        Node(E item){
            this.item = item;
            left = null;
            right = null;
        }
        public String toString(){
            return item.toString();
        }
    }
    String prettyTree() {
        if (root == null)
            return (" Empty tree\n");
        else
            return prettyTree( root, "" );
    }
    void insert(E item){
        Node<E> temp = new Node<>(item);
        root = merge(temp, root);
    }
    private Node<E> merge(Node<E> tree1, Node<E> tree2){
        Node<E> big;
        if(tree1 == null){
            return tree2;
        }
        if(tree2 == null){
            return tree1;
        }
        if(tree1.item.compareTo(tree2.item) > 0){
            tree1.right = merge(tree1.right, tree2);
            big = tree1;
        }
        else{
            tree2.right = merge(tree2.right, tree1);
            big = tree2;
        }
        if(notLeftist(big)){
            swapKids(big);
        }
        setNullPathLength(big);
        return big;
    }
    void clearHeap(){
        root = null;
    }
    void printMax(){
        if(root == null){
            System.out.println();
        }
        else{
            System.out.print(root);
        }
    }
    Node<E> deleteMax(){
        if(root == null){
            return null;
        }
        root = merge(root.left, root.right);
        return root;
    }
    private boolean notLeftist(Node<E> node){
        if(node.right != null && node.left != null){
            setNullPathLength(node.left);
            setNullPathLength(node.right);
            return node.left.nPL >= node.right.nPL;
        }
        if(node.left == null && node.right != null){
            return false;
        }
        return true;
    }
    private void swapKids(Node<E> node){
        Node<E> temp = node.left;
        node.left = node.right;
        node.right = temp;
    }
    private void setNullPathLength(Node<E> node){
        node.nPL = nPLSet(node);
    }
    private int nPLSet(Node<E> node){
        if(node.left == null || node.right == null){
            return 0;
        }
        return Math.min(nPLSet(node.left), nPLSet(node.right)) + 1;
    }
    private String prettyTree(Node<E> t, String indent) {
        StringBuilder tree = new StringBuilder();
        if(t == null){
            return "";
        }
        tree.append(prettyTree(t.right, indent + "  "));
        tree.append(indent);
        tree.append(t.toString());
        tree.append("\n");
        tree.append(prettyTree(t.left, indent + "  "));
        return tree.toString();
    }
}