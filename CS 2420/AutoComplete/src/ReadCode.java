import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;



public class ReadCode {
    private static int binarySearch(ArrayList<Term> list, int low, int high, String toFind){
        int x = low;
        int y = high;
        while(x <= y){
            int mid = (x + y) / 2;
            String temp = list.get(mid).getWord();
            int compare = toFind.compareTo(temp);
            if(compare == 0){
                return mid;
            }
            else if(compare < 0){
                y = mid -1 ;
            }
            else{
                x = mid + 1;
            }
        }
        return x;
    }
    //function to find where the accurances of a specific prefix start
    private static int findIndex(String toFind, ArrayList<Term> bigList){
        int index = binarySearch(bigList, 0, bigList.size() - 1, toFind);
        while(index > 0 && bigList.get(index).getWord().startsWith(toFind)){
            index --;
        }
        return index + 1;
    }
    //finds the end of a prefixes accurances
    private static int findLengthOfPrefixAccuance(String toFind, ArrayList<Term> bigList){
        int start = findIndex(toFind, bigList);
        int end = start;
        boolean endFound = false;
        while(end < bigList.size() && !endFound){
            if(bigList.get(end).getWord().startsWith(toFind)){
                end++;
            }
            else{
                endFound = true;
                //decrement because else the end will be off by 1
                end --;
            }
        }
        return end;
    }
    //the function that collects and then gives a set of auto complete options for a given int
    private static void autoComplete(String prefixInputs, int count, ArrayList<Term> termList, LeftistHeap<Term> h){
        String[] prefixes = prefixInputs.split(" ");
        //getting retrieving the terms from the term array list and then storing them in the heap
        for(int i = 0; i < prefixes.length; i += 2){
            int start = findIndex(prefixes[i], termList);
            int end = findLengthOfPrefixAccuance(prefixes[i], termList);
            for(int n = start; n <= end; n++){
                h.insert(termList.get(n));
            }
        }
        System.out.println("Prefix(es): " + prefixInputs + " Count: " + count);
        //now the heap is harvested for predictions
        for(int i = 0; i < count; i++){
            h.printMax();
            h.deleteMax();
        }
        System.out.println();
        h.clearHeap();
    }
    //main function for reading in the SortedWords.txt file and running functions to find "auto completes"
    public static void main(String[] args) {
        //heap and array list declarations
        LeftistHeap<Term> h = new LeftistHeap<>();
        ArrayList<Term> termList = new ArrayList<>();
        try {
            //setting the file name and instantiating a scanner to read the file
            String filename = "src\\SortedWords.txt";
            Scanner reader = new Scanner( new File( filename ) );
            //takes the first element of the SortedWords.txt, because it's simply the amount of words and can't be converted into a term
            reader.nextInt();
            //the read loop used to convert the contents of SortedWords.txt into an array list of terms
            while ((reader.hasNext())) {
                String word = reader.next();
                long freq = reader.nextInt();
                termList.add(new Term(word, freq));
             }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //test cases:
//      single letters:
        System.out.println("Single letter inquires: ");
        autoComplete("a", 5, termList, h);
        autoComplete("b", 5, termList, h);
        autoComplete("e", 5, termList, h);
        //multiple single letter inquiries:
        System.out.println("Multiple inputs for single letters (a | b | c | e)");
        autoComplete("a | b | c | e", 10, termList, h);
        //single prefix
        System.out.println("single prefixes: ");
        autoComplete("ac", 10, termList, h);
        //multi letter prefixes:
        System.out.println("multiple prefixes: ");
        autoComplete("acc | gr", 10, termList, h);
//        //getting input for the user
        System.out.println("now asking input from the user: ");
        Scanner userInput = new Scanner(System.in);
        System.out.print("Please enter a prefix (or more than one formatted like this: xxx | yyy | ...): ");
        String cmd = userInput.nextLine();
        System.out.print("Please enter a count : ");
        int countInput = userInput.nextInt();
        //calling the auto complete function
        autoComplete(cmd, countInput, termList, h);
    }
}