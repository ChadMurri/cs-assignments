public class Percolation {

    private boolean[][] opened;
    private int top = 0;
    private int bottom;
    private int size;
    private UnionFind unionFind;


    Percolation(int N) {
        size = N;
        bottom = size * size + 1;
        unionFind = new UnionFind(size * size + 2);
        opened = new boolean[size][size];
    }
    //opens a cell
    void open(int i, int n) {
        opened[i - 1][n - 1] = true;
        if (i == 1) {
            unionFind.union(UnionFindIndex(i, n), top);
        }
        if (i == size) {
            unionFind.union(UnionFindIndex(i, n), bottom);
        }

        if (n > 1 && isOpen(i, n - 1)) {
            unionFind.union(UnionFindIndex(i, n), UnionFindIndex(i, n - 1));
        }
        if (n < size && isOpen(i, n + 1)) {
            unionFind.union(UnionFindIndex(i, n), UnionFindIndex(i, n + 1));
        }
        if (i > 1 && isOpen(i - 1, n)) {
            unionFind.union(UnionFindIndex(i, n), UnionFindIndex(i - 1, n));
        }
        if (i < size && isOpen(i + 1, n)) {
            unionFind.union(UnionFindIndex(i, n), UnionFindIndex(i + 1, n));
        }
    }
    //checks if a spot is currently open or not
    boolean isOpen(int i, int j) {
        return opened[i - 1][j - 1];
    }

    //checks if the cell is connected to the top
    private boolean isFull(int i, int j) {
        if (0 < i && i <= size && 0 < j && j <= size) {
            return unionFind.connected(top, UnionFindIndex(i , j));
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
    //checks if a group is connected to the top and bottom
    boolean percolates() {
        return unionFind.connected(top, bottom);
    }
    //toString method for printing out the grid
    public String toString(){
        StringBuilder text = new StringBuilder();
        for(int i = 0; i < size; i++){
            for(int n = 0; n < size; n ++){
                if(isFull(i + 1, n + 1)){
                    text.append('\u2601');
                }
                else if(opened[i][n]){
                    text.append('\u25A0');
                }
                else{
                    text.append('\u25A2');
                }
            }
            text.append('\n');
        }
        return text.toString();
    }
    //coverts x and y into a one dimensional array index
    private int UnionFindIndex(int i, int n) {
        return size * (i - 1) + n;
    }
}