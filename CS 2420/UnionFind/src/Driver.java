
class Driver{
    public static void main(String[] args) {
        int size1 = 20;
        int n = 10000;
        int total = 0;
        Percolation hard = new Percolation(size1);
        int count = 0;
        while (!hard.percolates()) {
            int randX = (int) (Math.random() * size1) + 1;
            int randY = (int) (Math.random() * size1) + 1;
            if(!hard.isOpen(randX, randY)) {
                hard.open(randX, randY);
                count++;
            }
            if( count % 50 == 0){
                System.out.println("Grid after " + count + " cells have been opened: ");
                System.out.println(hard);
            }
        }
        System.out.println("Grid after percolation is possible:");
        System.out.println("Number of opens needed: " + count);
        System.out.println(hard);
        for(int i = 0; i < n; i++) {
            hard = new Percolation(size1);
            count = 0;
            while (!hard.percolates()) {
                int randX = (int) (Math.random() * size1) + 1;
                int randY = (int) (Math.random() * size1) + 1;
                if (!hard.isOpen(randX, randY)) {
                    hard.open(randX, randY);
                    count++;
                }
            }
            total += count;
        }
        System.out.println("Average cells needed to be opened (n = " + n + "): " + (double)total / n);
    }
}