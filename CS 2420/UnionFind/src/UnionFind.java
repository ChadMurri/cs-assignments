class UnionFind{
    private int[][] array;
    private int[] rank;
    private int[] parent;
    UnionFind(int number){
        array = new int[number][3];
        parent = new int[number];
        rank = new int[number];
        for(int i = 0; i < number; i++){
            parent[i] = i;
            rank[i] = 1;
        }
    }
    void printEntries(){
        for(int i = 0; i < array.length; i++){
            System.out.println(i + " Element: " + i + " Parent: " + parent[i] + " Size: " + rank[i]);
        }
    }
    //find
    public int find(int thing){
        while(thing != parent[thing]){
            parent[thing] = parent[parent[thing]];
            thing = parent[thing];
        }
        return thing;
    }
    //checks if a group is connected
    boolean connected(int thing, int otherThing){
        return find(thing) == find(otherThing);
    }
    //unions two groups
    void union(int t, int s){
        t = find(t);
        s = find(s);
        if(s == t){
            return;
        }
        if(rank[t] < rank[s]){
            parent[t] = s;
        }
        else if(rank[t] > rank[s]){
            parent[s] = t;
        }
        else{
            parent[s] = t;
            rank[t]++;
        }
    }
    //simple main for testing the UF
    public static void main(String[] args) {
        UnionFind test = new UnionFind(10);
        test.union(1, 0);
        test.union(2,3);
        test.union(4,5);
        test.union(1, 4);
        System.out.println(test.connected(1, 4));
        test.printEntries();
    }
}