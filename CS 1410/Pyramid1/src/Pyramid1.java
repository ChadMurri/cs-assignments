import java.util.Scanner;

public class Pyramid1
{
    public static void main(String[] args)
    {
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter the number of lines: ");
        long numberOfLines = userInput.nextInt();
        String temp = "" + numberOfLines;
        long roomForNums = temp.length();
        long space = (numberOfLines) * (roomForNums + 1) ;
        String line = "1";
        System.out.printf("%" + space + "s \n",line);
        long count = 1;
        while (count < numberOfLines)
        {
            count ++;
            space += roomForNums + 1;
            line =  String.format("%" + roomForNums + "d %" + roomForNums + "s %" + roomForNums + "d",count,line,count);
            System.out.printf("%" + space + "s \n",line);
        }
    }
}
