class Date {
    //public
    private int day = 1;
    private int month =1;
    private int year;
    private int offSet;
    private String[] monthName = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    Date(){
        year = setYear();
        offSet = setOffSet();
        dumbAssIdeaOfMine();
    }
    Date(int newYear, int newMonth, int newDay){
        offSet = setOffSet();
        day = newDay;
        month = newMonth;
        year = newYear;
    }
    void addDays(int days){
        int tempDay = day + days;
        while(getNumberOrDaysInMonth(year, month) < tempDay){
            tempDay = java.lang.Math.abs(tempDay - getNumberOrDaysInMonth(year, month));
            month ++;
            if(month > 12){
                month = 1;
                year++;
            }
        }
        day = tempDay;
    }
    void subtractDays(int days){
        int tempDay = day - days;
        while(1 > tempDay){
            month --;
            if(month < 1){
                month = 12;
                year --;
            }
            tempDay = getNumberOrDaysInMonth(year, month) + tempDay;
        }
        day = tempDay;
    }
    void printShortDate(){
        String zeroFill = "";
        String zeroFill2 = "";
        if (month < 10){
            zeroFill = "0";
        }
        if (day < 10){
            zeroFill2 = "0";
        }
        System.out.printf("%s%d/%s%d/%4d",zeroFill, month, zeroFill2, day, year);
    }
    void printLongDate(){
        System.out.printf("%-10s %2d, %d", getCurrentMonthName(), day, year);
    }
    String getCurrentMonthName(){
        return monthName[month -1];
    }
    int getCurrentMonth(){
        return month;
    }
    int getCurrentYear(){
        return year;
    }
    int getCurrentDayOfMonth(){
        return day;
    }
    boolean isLeapYear(){
        return isLeapYear(year);
    }
    //private
    boolean isLeapYear(int year){
        return false;
    }
    private  int getNumberOrDaysInMonth(int year, int month){
        int[] numDaysList = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if(isLeapYear(year)){
            numDaysList[1] = 29;
        }
        return numDaysList[month -1];
    }
    private long numOfDaysSince(){
        return (offSet + (System.currentTimeMillis() + java.util.TimeZone.getDefault().getRawOffset()) / (1000 * 3600 * 24));
    }
    private void dumbAssIdeaOfMine(){
        for(int i = 0; i < numOfDaysSince(); i ++){
            addDays(1);
        }
    }
    int setOffSet(){
        return 0;
    }
    int setYear(){
        return 1970;
    }
}