class GregorianDate extends Date{
    GregorianDate(){
    }
    GregorianDate(int year, int month, int day){
        super(year,month,day);
    }
    boolean isLeapYear(int year){
        boolean test = false;
        if (year % 4 == 0){
            test = true;
        }
        if (year % 100 == 0){
            test = false;
        }
        if (year % 400 == 0){
            test = true;
        }
        return test;
    }
}