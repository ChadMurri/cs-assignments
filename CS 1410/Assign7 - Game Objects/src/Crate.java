class Crate extends Entity{
    private Treasure inside;
    Crate(Treasure inside, int xCoordinate, int yCoordinate){
        super(xCoordinate,yCoordinate);
        this.inside = inside;
    }
    public String toString(){
        return String.format("Crate with %s at (%d, %d)", inside.getDescription(), gridLocation.x, gridLocation.y);
    }
    Treasure getTreasure(){
        return inside;
    }
}