class Entity{
    Position gridLocation;
    Entity(int xCoordinate, int yCoordinate){
        gridLocation = new Position(xCoordinate,yCoordinate);
    }
    public String toString(){
        return "Nothing";
    }
    void setPosition(int x, int y){
        gridLocation.x = x;
        gridLocation.y = y;
    }
    Position getPosition(){
        return gridLocation;
    }
    Treasure getTreasure(){
        return null;
    }
    String getColor(){
        return "none";
    }
}