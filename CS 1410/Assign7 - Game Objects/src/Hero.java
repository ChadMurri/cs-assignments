import java.util.ArrayList;

class Hero extends Entity{
    private String name;
    private ArrayList<Treasure> loot = new ArrayList<>();
    Hero(String name, int xCoordinate, int yCoordinate){
        super(xCoordinate,yCoordinate);
        this.name = name;
    }
    public String toString(){
        return String.format("%s standing at %s", name, gridLocation.toString());
    }
    String getName(){
        return name;
    }
    void attack(Entity victim){
        if(victim instanceof Crate){
            System.out.printf("%s crushed the crate into bits and found %s.\n", name, victim.getTreasure().getDescription());
            loot.add(victim.getTreasure());
        }
        if(victim instanceof Dragon){
            if(victim.getColor().equals("Golden")){
                loot.add(victim.getTreasure());
            }
            System.out.printf("%s bravely defeated the %s dragon!\n", name, victim.getColor());
        }
    }
    ArrayList<Treasure> getTreasures(){
        return loot;
    }
}