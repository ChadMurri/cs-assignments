enum Treasure{
    Wood("Wood", 0),
    Statue("Statue", 1),
    Coins("Coins", 2),
    Food("Food", 3),
    Rags("Rags", 4);
    private String description;
    private int treasureIndex;
    Treasure(String desc, int index){
        description = desc;
        treasureIndex = index;
    }
    String getDescription(){
        return description;
    }
    int getTreasureIndex(){
        return treasureIndex;
    }
}