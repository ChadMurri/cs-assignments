class Dragon extends Entity{
    private String color;
    Dragon(String color, int xCoordinate, int yCoordinate){
        super(xCoordinate,yCoordinate);
        this.color = color;
    }
    public String toString(){
        return String.format("The %s dragon breathing fire at %s", color, gridLocation.toString());
    }
    String getColor(){
        return color;
    }
    Treasure getTreasure(){
        return Treasure.Coins;
    }
}