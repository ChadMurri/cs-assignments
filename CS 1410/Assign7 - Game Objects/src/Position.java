class Position{
    int x;
    int y;
    Position(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return String.format("(%d, %d)", x, y);
    }
}