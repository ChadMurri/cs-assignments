import java.util.ArrayList;
class Arena{
    private int entityCount = 0;
    private int[] treasureCount = {0,0,0,0,0};
    private int dragonCount = 0;
    private Hero arenaHero = null;
    private ArrayList<Entity[]> grid;
    private int width;
    private int height;
    Arena(int width, int height){
        this.width = width;
        this.height = height;
        grid = new ArrayList<>(width);
        for(int i = 0; i < width; i ++){
            grid.add(new Entity[height]);
            for( int n = 0; n < height; n ++){
                grid.get(i)[n] = null;
            }
        }
    }
    int getEntityCount(){
        return entityCount;
    }
    int getDragonCount(){
        return dragonCount;
    }
    int getTreasureCount(Treasure ent){
        return treasureCount[ent.getTreasureIndex()];
    }
    void moveHero(int newX, int newY){
        grid.get(arenaHero.gridLocation.x)[arenaHero.gridLocation.y] = null;
        arenaHero.gridLocation.x = newX;
        arenaHero.gridLocation.y = newY;
        if(grid.get(newX)[newY] != null){
            if(grid.get(newX)[newY] instanceof Crate){
                treasureCount[grid.get(newX)[newY].getTreasure().getTreasureIndex()] --;
            }
            if(grid.get(newX)[newY] instanceof Dragon){
                dragonCount --;
            }
            arenaHero.attack(grid.get(newX)[newY]);
            entityCount --;
        }
        grid.get(newX)[newY] = arenaHero;
        System.out.printf("%s moved to (%d, %d)\n", arenaHero.getName(), arenaHero.gridLocation.x, arenaHero.gridLocation.y);
    }
    void reportHero(){
        System.out.printf("=== Hero report for %s ---\n", arenaHero.getName());
        System.out.printf("Position: (%d, %d)\n", arenaHero.gridLocation.x, arenaHero.gridLocation.y);
        System.out.println("Treasures:");
        for(Treasure i: arenaHero.getTreasures()){
            System.out.println("  " + i.getDescription());
        }
        System.out.println();
    }
    boolean add(Entity ent){
        boolean test = false;
        if(ent.gridLocation.x >= 0 && ent.gridLocation.x <= width -1 && ent.gridLocation.y >= 0 && ent.gridLocation.y <= height - 1) {
            if (grid.get(ent.gridLocation.x)[ent.gridLocation.y] == null) {
                if (ent instanceof Hero) {
                    if (arenaHero == null) {
                        arenaHero = (Hero) ent;
                        grid.get(ent.gridLocation.x)[ent.gridLocation.y] = ent;
                        System.out.printf("Successfully added \'%s\' to the arena\n", ent.toString());
                        entityCount++;
                        test = true;
                    } else {
                        System.out.printf("Could not add \'%s\' because there is already a hero in the arena.\n", ent.toString());
                    }
                } else {
                    grid.get(ent.gridLocation.x)[ent.gridLocation.y] = ent;
                    System.out.printf("Successfully added \'%s\' to the arena\n", ent.toString());
                    entityCount++;
                    test = true;
                    if (ent instanceof Dragon) {
                        dragonCount++;
                    }
                    if (ent instanceof Crate) {
                        treasureCount[ent.getTreasure().getTreasureIndex()]++;
                    }
                }
            } else {
                System.out.printf("Could not add \'%s\' because another entity is already there.\n", ent.toString());
            }
        }
        else{
            System.out.printf("Could not add \'%s\' because it was placed out of bounds.\n", ent.toString());
        }
        return test;
    }
    ArrayList<Entity[]> getGrid(){
        return grid;
    }
    Hero getHero(){
        return arenaHero;
    }
}