public class Inside {
    static Boolean isPointInsideCircle(double ptX, double ptY, double circleY, double circleX, circleRadius) {
        double leftSideCircleEqx = (ptX - circleX) * (ptX - circleX) + (ptY - circleY) * (ptY - circleY);
        double rightSideCircleEqx = circleRadius * circleRadius;
        if (leftSideCircleEqx <= rightSideCircleEqx) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {

    }
}

