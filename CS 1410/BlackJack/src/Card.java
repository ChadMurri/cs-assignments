class Cards{
    private CardVal cardValue;
    private String suit;
    Cards(String suit, CardVal card){
        this.suit = suit;
        this.cardValue = card;
    }
    public String toString(){
        return String.format("The %s of %s.", cardValue.getName(), suit);
    }
}