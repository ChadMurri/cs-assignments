enum CardVal{
    Ace("Ace", 1),
    Two("Two", 2),
    Three("Three", 3),
    Four("Four", 4),
    Five("Five", 5),
    Six("Six", 6),
    Seven("Seven", 7),
    Eight("Eight", 8),
    Nine("Nine", 9),
    Ten("Ten", 10),
    Jack("Jack", 10),
    Queen("Queen", 10),
    King("King", 10);

    String name;
    int value;
    CardVal(String name, int value){
        this.name = name;
        this.value = value;
    }
    int getValue(){
        return value;
    }
    String getName(){
        return name;
    }
}