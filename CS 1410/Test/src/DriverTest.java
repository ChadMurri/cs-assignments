
public class DriverTest{

    private static void reportHero(){
        String[] treasure = {
                "Wood",
                "Rags",
                "Coins"
        };
        System.out.printf("=== Hero report for %s ---", "Chad");
        System.out.printf("Position: (%d, %d)", 1, 3);
        System.out.println("Treasures:");
        for(String i: treasure){
            System.out.println("  " + i);
        }
    }
    public static void main(String[] args) {
        reportHero();
    }
}