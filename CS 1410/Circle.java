public class Circle {

    public static Circle createCircleByDiameter(double diameter) {
        return new Circle(diameter / 2.0);
    }

    public Circle() {
        radius = 1.0;
        howManyCreated++;
    }

    public Circle(double radius) {
        this.radius = radius;
        howManyCreated++;
    }

    public static int getHowManyCreated() {
        return howManyCreated;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public double getPerimeter() {
        return Math.PI * radius * 2;
    }

    public void setPerimeter(double newPerimeter) {
        if (newPerimeter > 0) {
            radius = newPerimeter / (Math.PI * 2);
        }
    }

    public void setDiameter(double newDiameter) {
        if (newDiameter > 0) {
            //radius = newDiameter / 2.0;
            setRadius(newDiameter / 2.0);
        }
    }

    public void setRadius(double newRadius) {
        if (newRadius <= 0.0) {
            // Generate some error
        }
        else {
            radius = newRadius;
        }
    }

    public double getRadius() {
        return radius;
    }

    private double radius;
    private static int howManyCreated = 0;
}

