import java.util.Scanner;
import java.lang.Math;

public class Pyramid2
{
    public static void main(String[] args)
    {
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter the number of lines: ");
        long numberOfLines = userInput.nextInt();
        String temp = "" + Math.pow(2,numberOfLines);
        long roomForNums = temp.length();
        long space = (numberOfLines) * (roomForNums + 1) ;
        String line = "1";
        String leftLine = "1";
        String rightLine = "1";
        long exponential = 1;
        System.out.printf("%" + space + "s \n",line);
        long count = 1;
        while (count < numberOfLines)
        {
            count ++;
            space += roomForNums + 1;
            line =  String.format("%" + roomForNums + "s %" + roomForNums + "d %" + roomForNums + "s",leftLine, (exponential * 2),rightLine);
            exponential *= 2;
            leftLine = String.format("%" + roomForNums + "s %" + roomForNums +"d",leftLine,exponential);
            rightLine = String.format("%" + roomForNums + "d %" + roomForNums +"s",exponential,rightLine);
            System.out.printf("%" + space + "s \n",line);
        }
    }
}