import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

public class SpellChecker {

    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        ArrayList<String> letter = new ArrayList<>();
        BinarySearchTree<String> dictionary = new BinarySearchTree<>();
        testTree();

        try {
            Scanner input = new Scanner(new File("Dictionary.txt"));
            while (input.hasNext()) {
                String line = input.next().toLowerCase();
                words.add(line);
            }
        } catch (java.io.IOException ex) {
            System.out.println("File exception: " + ex.getMessage());
        }
        try {
            Scanner input = new Scanner(new File("Letter.txt"));
            while (input.hasNext()) {
                String line = input.next().toLowerCase();
                line = line.replaceAll("[\\[()\\]{},.+\\\\-]", "").replaceAll(" +", " ");
                letter.add(line);
            }
        } catch (java.io.IOException ex) {
            System.out.println("File exception: " + ex.getMessage());
        }

        java.util.Collections.shuffle(words, new java.util.Random(System.currentTimeMillis()));
        for(String i: words){
            dictionary.insert(i);
        }
        reportTreeStats(dictionary);
        System.out.println("Misspelled words:");
        for(String i : letter){
            if(!dictionary.search(i)){
                System.out.printf("%s\n", i);
            }
        }
    }
    private static void testTree() {
        BinarySearchTree<String> tree = new BinarySearchTree<>();

        //
        // Add a bunch of values to the tree
        tree.insert("Olga");
        tree.insert("Tomeka");
        tree.insert("Benjamin");
        tree.insert("Ulysses");
        tree.insert("Tanesha");
        tree.insert("Judie");
        tree.insert("Tisa");
        tree.insert("Santiago");
        tree.insert("Chia");
        tree.insert("Arden");

        //
        // Make sure it displays in sorted order
        tree.display("--- Initial Tree State ---");
        reportTreeStats(tree);

        //
        // Try to add a duplicate
        if (tree.insert("Tomeka")) {
            System.out.println("oops, shouldn't have returned true from the insert");
        }
        tree.display("--- After Adding Duplicate ---");
        reportTreeStats(tree);

        //
        // Remove some existing values from the tree
        tree.remove("Olga");    // Root node
        tree.remove("Arden");   // a leaf node
        tree.display("--- Removing Existing Values ---");
        reportTreeStats(tree);

        //
        // Remove a value that was never in the tree, hope it doesn't crash!
        tree.remove("Karl");
        tree.display("--- Removing A Non-Existent Value ---");
        reportTreeStats(tree);
    }

    private static void reportTreeStats(BinarySearchTree<String> tree) {
        System.out.println("-- Tree Stats --");
        System.out.printf("Total Nodes : %d\n", tree.numberNodes());
        System.out.printf("Leaf Nodes  : %d\n", tree.numberLeafNodes());
        System.out.printf("Tree Height : %d\n", tree.height());
    }

}
