
class BinarySearchTree <E extends Comparable<E>>{
    private TreeNode root;
    private int numberNodes = 0;

    class TreeNode{
        E value;
        TreeNode left;
        TreeNode right;
        TreeNode(E value){
            this.value = value;
        }
    }
    int numberNodes(){
        return numberNodes;
    }
    int numberLeafNodes(){
        return recursiveLeafCounter(root);
    }
    private int recursiveLeafCounter(TreeNode value){
        if(value == null){
            return 0;
        }
        if(value.right == null && value.left == null){
            return 1;
        }
        return recursiveLeafCounter(value.right) + recursiveLeafCounter(value.left);
    }
    int height(){
        return recursiveHeight(root) - 1;
    }
    private int recursiveHeight(TreeNode value){
        if(value == null){
            return 0;
        }
        return 1 + java.lang.Math.max(recursiveHeight(value.left),recursiveHeight(value.right));
    }
    boolean insert(E value) {
        if(search(value)){
            return false;
        }
        if (root == null) {
            root = new TreeNode(value);
        }
        else {
            TreeNode parent = null;
            TreeNode node = root;

            while (node != null) {
                parent = node;
                if (node.value.compareTo(value) < 0) {
                    node = node.right;
                }
                else {
                    node = node.left;
                }
            }

            TreeNode newNode = new TreeNode(value);
            if (parent.value.compareTo(value) < 0) {
                parent.right = newNode;
            }
            else {
                parent.left = newNode;
            }
        }
        numberNodes ++;
        return true;
    }
    boolean search(E value) {
        if (root == null) {
            return false;
        }
        else {
            TreeNode node = root;

            while (node != null) {
                if (node.value.compareTo(value) < 0) {
                    node = node.right;
                }
                else if(node.value.compareTo(value) > 0){
                    node = node.left;
                }
                else{
                    return true;
                }
            }
        }
        return false;
    }
    boolean remove(E value) {
        TreeNode parent = null;
        TreeNode node = root;
        boolean done = false;
        if(!search(value)){
            return false;
        }
        while (!done) {
                if (node.value.compareTo(value) < 0) {
                    parent = node;
                    node = node.right;
                } else if (node.value.compareTo(value) > 0) {
                    parent = node;
                    node = node.left;
                } else {
                    done = true;
                }
        }

        // Case for the no left child
        if (node.left == null) {
            if (parent == null) {
                root = node.right;
            }
            else {
                if (parent.value.compareTo(value) < 0) {
                    parent.right = node.right;
                }
                else {
                    parent.left = node.right;
                }
            }
        }
        else {
            TreeNode parentOfRight = node;
            TreeNode rightMost = node.left;
            while (rightMost.right != null) {
                parentOfRight = rightMost;
                rightMost = rightMost.right;
            }
            node.value = rightMost.value;
            if (parentOfRight.right == rightMost) {
                parentOfRight.right = rightMost.left;
            }
            else {
                parentOfRight.left = rightMost.left;
            }
        }
        numberNodes --;
        return true;
    }
    void display(String msg) {
        System.out.println(msg);
        traverseInOrder(this.root);
    }
    private void traverseInOrder(TreeNode node) {
        if (node != null) {
            traverseInOrder(node.left);
            System.out.println(node.value);
            traverseInOrder(node.right);
        }
    }

}