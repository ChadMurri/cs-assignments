/**
 * Assignment 4 for CS 1410
 * This program evaluates the bubble and selection sorts versus each other.
 *
 * @author James Dean Mathias
 */

public class SortingDriver {
    public static SortingStats bubbleSort(int[] data){
        SortingStats sort = new SortingStats();
        int temp = 0;
        long totalTime = 0;
        long timeEnd = 0;
        long timeStart = System.currentTimeMillis();
        for(int i = 0; i < data.length; i ++){
            for(int n = 0; n <data.length - 1 - i; n ++){
                sort.incrementCompareCount();
                if (data[n] > data[n + 1]){
                 temp = data[n];
                 data[n] = data[n+1];
                 data[n+1] = temp;
                 sort.incrementSwapCount();
                }
            }
        }
        timeEnd = System.currentTimeMillis();
        totalTime = timeEnd - timeStart;
        sort.setTime(totalTime);
        return sort;
    }
    public static SortingStats selectionSort(int[] data){
        SortingStats sort = new SortingStats();
        int temp = 0;
        int minVal = 0;
        int minIndex = 0;
        long totalTime = 0;
        long timeEnd = 0;
        long timeStart = System.currentTimeMillis();
        for(int i = 0; i < data.length; i++){
            minVal = data[i];
            minIndex = i;
            for(int n = i; n < data.length; n ++){
                sort.incrementCompareCount();
                if (data[n] > minVal){
                    minVal = data[n];
                    minIndex = n;
                    sort.incrementCompareCount();
                }
            }
            if (minVal < data[i]){
                temp = data[i];
                data[i] = data[minIndex];
                data[minIndex] = temp;
            }
        }
        timeEnd = System.currentTimeMillis();
        totalTime = timeEnd - timeStart;
        sort.setTime(totalTime);
        return sort;

    }
    public static int[] generateNumbers(int howMany){
        int [] list = new int[howMany];
        double rando = 0;
        for (int i=0; i<howMany; i++){
            rando = java.lang.Math.random() * 100000;
            list[i] = (int)rando;
        }
        return list;
    }
    public static void main(String[] args) {
        SortingStats bubble = new SortingStats();
        SortingStats selection = new SortingStats();
        System.out.println("--- Timing Results ---\n");
        for(int i = 1000; i <= 10000; i += 1000){
            int [] bubbleList = generateNumbers(i);
            int [] selectionList = generateNumbers(i);
            bubble =bubbleSort(bubbleList);
            selection = selectionSort(selectionList);
            System.out.println("Number of items      : " + i);
            System.out.println("Bubble sort time     : " + bubble.getTimeInMs());
            System.out.println("Selection sort time  : " + selection.getTimeInMs());
            System.out.println();
        }

    }

}
