import java.util.Scanner;
public class ISBN
{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the first 9 digits of an ISBN: ");
        long isbnInput = input.nextLong();
        long isbnTemp = isbnInput;
        long ISBN10 = 0;
        int remainder = 0;
        int count = 0;
        long d9 = isbnTemp % 10;
        isbnTemp /= 10;
        long d8 = isbnTemp % 10;
        isbnTemp /= 10;
        long d7 = isbnTemp % 10;
        isbnTemp /= 10;
        long d6 = isbnTemp % 10;
        isbnTemp /= 10;
        long d5 = isbnTemp % 10;
        isbnTemp /= 10;
        long d4 = isbnTemp % 10;
        isbnTemp /= 10;
        long d3 = isbnTemp % 10;
        isbnTemp /= 10;
        long d2 = isbnTemp % 10;
        isbnTemp /= 10;
        long d1 = isbnTemp % 10;
        isbnTemp /= 10;
        ISBN10 = (d1 * 1 + d2 * 2 + d3 * 3 + d4 * 4 + d5 * 5 + d6 * 6 + d7 * 7 + d8 * 8 + d9 * 9) % 11;
        if (ISBN10 == 10)
        {
            System.out.println("The ISBN-10 number is: " + isbnInput + "X");
        }
        else
            {
                System.out.print("The ISBN-10 number is: " + isbnInput);
                System.out.println(ISBN10);
            }
        }


}