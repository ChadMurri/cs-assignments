import java.util.Scanner;
import java.lang.Math;
public class Quadratic
{
    public static void main(String[] args) {
        double rootOne = 0;
        double rootTwo = 0;
        double determinate = 0;
        double complex = 0;
        double a = 0;
        double b = 0;
        double c = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a, b, c: ");
        a = input.nextDouble();
        b = input.nextDouble();
        c = input.nextDouble();
        determinate = b*b - 4*a*c;
        if (determinate > 0)
        {
            rootOne = (-b + Math.sqrt(determinate)) / 2 * a;
            rootTwo = (-b - Math.sqrt(determinate)) / 2 * a;
            System.out.println("There are two roots for the quadratic equation with these coefficients.");
            System.out.println("r1 = " + rootOne);
            System.out.println("r2 = " + rootTwo);
        }
        if (determinate == 0)
        {
            rootOne = -b / 2*a;
            System.out.println("There is one root for the quadratic equation with these coefficients.");
            System.out.println("r1 = " + rootOne);
        }
        if (determinate < 0)
        {
            rootOne = -b / 2 * a;
            complex = Math.sqrt(Math.abs(determinate))/2*a;
            System.out.println("There are two complex roots for the quadratic equation with these coefficients.");
            System.out.println("r1 = " + rootOne + " +- " + complex + "i");
        }
    }
}
