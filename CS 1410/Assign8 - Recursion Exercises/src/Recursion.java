public class Recursion {
    static long arraySum(int [] data, int position){
        if (position >= data.length){
            return 0;
        }
        return data[position] + arraySum(data, position +1);
    }
    static int arrayMin(int[] data, int position){
        return findMin(data, position, data[position]);
    }
    private static int findMin(int[] data, int position, int currentMin){
        if(position > data.length - 1){
            return currentMin;
        }
        if(currentMin > data[position]){
            currentMin = data[position];
        }
        return findMin(data, position + 1, currentMin);
    }
    static boolean isWordSymmetric(String[] words, int start, int end){
        if(start >= end){
            return true;
        }
        if(words[start].toLowerCase().equals(words[end].toLowerCase())){
            return isWordSymmetric(words,start + 1, end - 1);
        }
        else{
            return false;
        }
    }
    static double computePyramidWeights(double[][] weights, int row, int column){
        if(row < weights.length && row >= 0 && column < weights[row].length && column >= 0) {
            return weights[row][column] + (.5) * computePyramidWeights(weights, row - 1, column - 1) + (.5) * computePyramidWeights(weights, row - 1, column);
        }
        else {
            return 0;
        }
    }
    static int howManyOrganisms(char[][] image){
        int numOfOrganisms = 0;
        char label = '`';
        for(int i = 0; i < image.length; i ++){
            for(int n = 0; n < image[i].length; n ++){
                if(image[i][n] == '*'){
                    numOfOrganisms ++;
                    label ++;
                }
                isThisAnewOrganism(image, i, n, label);
            }
        }
        return numOfOrganisms;
    }
    private static void isThisAnewOrganism(char[][] image, int row, int column, char label){
        if(row >= 0 && row < image.length && column >= 0 && column < image[row].length){
            if(image[row][column] == '*'){
                image[row][column] = label;
                isThisAnewOrganism(image, row - 1, column, label);
                isThisAnewOrganism(image, row + 1, column, label);
                isThisAnewOrganism(image, row, column - 1, label);
                isThisAnewOrganism(image, row, column + 1, label);
            }
        }
    }
    public static void main(String[] args) {

        int[] sumMe = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 };
        System.out.printf("Array Sum: %d\n", arraySum(sumMe, 0));

        int[] minMe = { 0, 1, 1, 2, 3, 5, 8, -42, 13, 21, 34, 55, 89 };
        System.out.printf("Array Min: %d\n", arrayMin(minMe, 0));

        String[] amISymmetric =  {
                "You can cage a swallow can't you but you can't swallow a cage can you",
                "I still say cS 1410 is my favorite class"
        };
        for (String test : amISymmetric) {
            String[] words = test.toLowerCase().split(" ");
            System.out.println();
            System.out.println(test);
            System.out.printf("Is word symmetric: %b\n", isWordSymmetric(words, 0, words.length - 1));
        }

        double weights[][] = {
                { 51.18 },
                { 55.90, 131.25 },
                { 69.05, 133.66, 132.82 },
                { 53.43, 139.61, 134.06, 121.63 }
        };
        System.out.println();
        System.out.println("--- Weight Pyramid ---");
        for (int row = 0; row < weights.length; row++) {
            for (int column = 0; column < weights[row].length; column++) {
                double weight = computePyramidWeights(weights, row, column);
                System.out.printf("%.2f ", weight);
            }
            System.out.println();
        }

        char image[][] = {
                {'*','*',' ',' ',' ',' ',' ',' ','*',' '},
                {' ','*',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ',' ',' ',' ','*','*',' ',' '},
                {' ','*',' ',' ','*','*','*',' ',' ',' '},
                {' ','*','*',' ','*',' ','*',' ','*',' '},
                {' ','*','*',' ','*','*','*','*','*','*'},
                {' ',' ',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ',' ',' ',' ',' ',' ','*',' '},
                {' ',' ',' ','*','*','*',' ',' ','*',' '},
                {' ',' ',' ',' ',' ','*',' ',' ','*',' '}
        };
        int howMany = howManyOrganisms(image);
        System.out.println();
        System.out.println("--- Labeled Organism Image ---");
        for (char[] line : image) {
            for (char item : line) {
                System.out.print(item);
            }
            System.out.println();
        }
        System.out.printf("There are %d organisms in the image.\n", howMany);

    }

}
