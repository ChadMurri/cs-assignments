public class BinaryTreeDemo {
    public static void main(String[] args) {

        BinarySearchTree myTree = new BinarySearchTree();

        myTree.insert("Logan");
        myTree.insert("Hyde Park");
        myTree.insert("Smithfield");
        myTree.insert("Millville");
        myTree.insert("Mendon");
        myTree.insert("Nibley");
        myTree.insert("Cache Valley");
        myTree.insert("Lewiston");
        myTree.insert("Richmond");

        myTree.remove("Logan");

        myTree.traverseInOrder();

        try {
            java.util.Scanner input = new java.util.Scanner(new java.io.File("myFile.txt"));

            while (input.hasNext()) {
                String line = input.next();
                myTree.insert(line);
            }
        }
        catch (java.io.IOException ex) {
            System.out.println("File exception: " + ex.getMessage());
        }
    }
}
