public class Chapter9 {
    public static void main(String[] args) {

//        Circle c1 = new Circle();
//        Circle c2 = new Circle(2.22);
//
//        System.out.printf("(c1) Area of a circle with radius %.2f is %.2f\n",
//                c1.getRadius(), c1.getArea());
//        System.out.printf("(c2) Area of a circle with radius %.2f is %.2f\n",
//                c2.getRadius(), c2.getArea());
//
//        c1 = c2;
//
//        System.out.printf("(c1) Area of a circle with radius %.2f is %.2f\n",
//                c1.getRadius(), c1.getArea());
//        System.out.printf("(c2) Area of a circle with radius %.2f is %.2f\n",
//                c2.getRadius(), c2.getArea());
//
//        c1.setRadius(3.33);
//
//        System.out.printf("(c1) Area of a circle with radius %.2f is %.2f\n",
//                c1.getRadius(), c1.getArea());
//        System.out.printf("(c2) Area of a circle with radius %.2f is %.2f\n",
//                c2.getRadius(), c2.getArea());
//
//        System.gc();

        Circle c1 = new Circle();
        Circle c2 = new Circle(2.22);
//        System.out.printf("There have been %d circles created.\n",
//                Circle.getHowManyCreated());
//
//        Circle c3 = new Circle(3.33);
//        System.out.printf("There have been %d circles created.\n",
//                c1.getHowManyCreated());
//
//        Circle c4 = Circle.createCircleByDiameter(4.44);
//        reportCircle(c1);
//        doubleCircle(c1);
//        reportCircle(c1);

        Circle[] data = new Circle[4];
        for (int circle = 0; circle < data.length; circle++) {
            data[circle] = new Circle(circle + 1);
        }

//        for (Circle c : data) {
//            reportCircle(c);
//        }

        for (int circle = 0; circle < data.length; circle++) {
            reportCircle(data[circle]);
        }

        reportTotalArea(data);
    }

    public static void reportTotalArea(Circle[] circles) {
        double totalArea = 0;
//        for (int circle = 0; circle < circles.length; circle++) {
//            totalArea += circles[circle].getArea();
//        }
        for (Circle c : circles) {
            totalArea += c.getArea();
        }
        System.out.printf("Total area of all circles: %.2f\n", totalArea);
    }

    public static void reportCircle(Circle c) {
        System.out.println("--- Circle Report ---");
        System.out.printf("    Radius    : %.2f\n", c.getRadius());
        System.out.printf("    Area      : %.2f\n", c.getArea());
        System.out.printf("    Perimeter : %.2f\n", c.getPerimeter());
    }

    public static void doubleCircle(Circle c) {
        c.setRadius(c.getRadius() * 2);
    }
}
