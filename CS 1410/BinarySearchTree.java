public class BinarySearchree {
    private TreeNode root;

    public boolean find(String value) {
        boolean found = false;
        TreeNode node = root;

        while (!found && node != null) {
            if (node.value.equals(value)) {
                found = true;
            }
            else if (node.value.compareTo(value) < 0) {
                node = node.right;
            }
            else {
                node = node.left;
            }
        }

        return found;
    }

    public void insert(String value) {
        if (root == null) {
            root = new TreeNode(value);
        }
        else {
            TreeNode parent = null;
            TreeNode node = root;

            while (node != null) {
                parent = node;
                if (node.value.compareTo(value) < 0) {
                    node = node.right;
                }
                else {
                    node = node.left;
                }
            }

            TreeNode newNode = new TreeNode(value);
            if (parent.value.compareTo(value) < 0) {
                parent.right = newNode;
            }
            else {
                parent.left = newNode;
            }
        }
    }

    public void traverseInOrder() {
        traverseInOrder(this.root);
    }
    private void traverseInOrder(TreeNode node) {
        if (node != null) {
            traverseInOrder(node.left);
            System.out.println(node.value);
            traverseInOrder(node.right);
        }
    }

    public void remove(String value) {
        TreeNode parent = null;
        TreeNode node = root;
        boolean done = false;
        while (!done) {
            if (node.value.compareTo(value) < 0) {
                parent = node;
                node = node.right;
            }
            else if (node.value.compareTo(value) > 0) {
                parent = node;
                node = node.left;
            }
            else {
                done = true;
            }
        }

        // Case for the no left child
        if (node.left == null) {
            if (parent == null) {
                root = node.right;
            }
            else {
                if (parent.value.compareTo(value) < 0) {
                    parent.right = node.right;
                }
                else {
                    parent.left = node.right;
                }
            }
        }
        else {
            TreeNode parentOfRight = node;
            TreeNode rightMost = node.left;
            while (rightMost.right != null) {
                parentOfRight = rightMost;
                rightMost = rightMost.right;
            }
            node.value = rightMost.value;
            if (parentOfRight.right == rightMost) {
                parentOfRight.right = rightMost.left;
            }
            else {
                parentOfRight.left = rightMost.left;
            }
        }
    }

    private class TreeNode {
        public String value;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(String value) {
            this.value = value;
        }
    }
}
