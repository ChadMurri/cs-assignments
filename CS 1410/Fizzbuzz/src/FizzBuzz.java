class FizzBuzz{
    private int[] multiples;
    private String[] insertWords;
    FizzBuzz(int[] multiples, String[] insertWords){
        this.insertWords = insertWords;
        this.multiples = multiples;
    }
    void game(int numberOfTurns){
        boolean flag = false;
        for(int i = 1; i < numberOfTurns + 1; i++){
            for(int n = 0; n < multiples.length; n ++){
                if(i % multiples[n] == 0){
                    System.out.print(insertWords[n]);
                    flag = true;
                }
            }
            if(!flag){
                System.out.println(i);
            }
            else{
                System.out.println("!");
            }
            flag = false;
        }
    }
}
