/**
 * Assignment 3 for CS 1410
 * This program determines if points are contained within circles or rectangles.
 *
 * @author James Dean Mathias
 */
public class Inside {
    /**
     * This is the primary driver code to test the "inside" capabilities of the
     * various functions.
     */
    static void reportPoint(double x, double y){
        System.out.print("Point(" + x + ", " + y +") ");
    }
    static void reportCircle(double x, double y, double r){
        System.out.print("Circle(" + x + ", " + y + ") Radius: " + r + "\n");
    }
    static void reportRectangle(double left, double top, double width, double height){
        System.out.print("Rectangle(" + left +", " + top + ", " + width + ", " + height + ")\n");
    }
    static boolean isPointInsideCircle(double ptX, double ptY, double circleX, double circleY, double circleRadius){
        double leftSideCircleEq = (ptX - circleX)*(ptX - circleX) + (ptY - circleY)*(ptY - circleY);
        double rightSideCircleEq = circleRadius*circleRadius;
        if (leftSideCircleEq <= rightSideCircleEq){
            return true;
        }
        else{
            return false;
        }
    }
    static boolean isPointInsideRectangle(double ptX, double ptY, double rLeft, double rTop, double rWidth, double rHeight){
        double xRight = rLeft + rWidth;
        double yBottom = rTop - rHeight;
        if (ptX >= rLeft && ptX >= xRight && ptY <= rTop && ptY >= yBottom){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        double[] ptX = { 1, 2, 3, 4 };
        double[] ptY = { 1, 2, 3, 4 };
        double[] circleX = { 0, 5 };
        double[] circleY = { 0, 5 };
        double[] circleRadius = { 3, 3 };
        double[] rectLeft = { -2.5, -2.5 };
        double[] rectTop = { 2.5, 5.0 };
        double[] rectWidth = { 6.0, 5.0 };
        double[] rectHeight = { 5.0, 2.5 };
        System.out.println("--- Report of Points and Circles ---\n");
        for (int n = 0; n < circleX.length; n++){
            for (int i = 0;i < ptX.length; i++){
                reportPoint(ptX[i],ptY[i]);
                if (isPointInsideCircle(ptX[i],ptY[i], circleY[n],circleX[n],circleRadius[n])){
                    System.out.print("is inside ");
                }
                else{
                    System.out.print("is outside ");
                }
                 reportCircle(circleX[n],circleY[n],circleRadius[n]);
            }
        }
        System.out.println("\n--- Report of Points and Rectangles ---\n");
        for (int n = 0; n < rectLeft.length; n++) {
            for (int i = 0; i < ptX.length; i++) {
                reportPoint(ptX[i], ptY[i]);
                if (isPointInsideRectangle(ptX[i], ptY[i], rectLeft[n], rectTop[n], rectWidth[n], rectHeight[n])) {
                    System.out.print("is inside ");
                } else {
                    System.out.print("is outside ");
                }
                reportRectangle(rectLeft[n], rectTop[n], rectWidth[n], rectHeight[n]);
            }
        }
    }

}
